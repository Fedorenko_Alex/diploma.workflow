#!/bin/sh

# If you would like to do some extra provisioning you may
# add any commands you wish to this file and they will
# be run after the Homestead machine is provisioned.
#
# If you have user-specific configurations you would like
# to apply, you may also create user-customizations.sh,
# which will be run after this script.
if ! grep "short_open_tag = On" /etc/php/7.3/fpm/php.ini
then
	sudo cp -p /etc/php/7.3/fpm/php.ini /etc/php/7.3/fpm/php.ini.bak
	sudo sed -i '/short_open_tag = Off/c short_open_tag = On' /etc/php/7.3/fpm/php.ini
fi

if ! grep "xdebug.remote_autostart=1" /etc/php/7.3/fpm/conf.d/20-xdebug.ini
then
	sudo cp -p /etc/php/7.3/fpm/conf.d/20-xdebug.ini /etc/php/7.3/fpm/conf.d/20-xdebug.ini.bak
	sudo sed -i '/xdebug.remote_connect_back = 1/c xdebug.remote_autostart = 1\nxdebug.remote_host = 10.0.2.2' /etc/php/7.3/fpm/conf.d/20-xdebug.ini
fi

sudo service php7.3-fpm restart