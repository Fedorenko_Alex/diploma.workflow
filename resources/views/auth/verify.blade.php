@extends('home')

@section('content')
    <div class="container mt-5">
    @if (session('resent'))
        <div class="alert alert-success" role="alert">
            <p>@lang('auth.verification_sent')</p>
            <hr>
            <p class="mb-0">@lang('auth.verification_footer') <a href="{{ route('verification.resend') }}">@lang('auth.verification_footer_link')</a></p>
        </div>
    @else

        <div class="alert alert-warning" role="alert">
            <h4 class="alert-heading">@lang('auth.verification_header')</h4>
            <p>  @lang('auth.verification_main')</p>
            <hr>
            <p class="mb-0">@lang('auth.verification_footer') <a href="{{ route('verification.resend') }}">@lang('auth.verification_footer_link')</a></p>
        </div>
    @endif
    </div>
@endsection
