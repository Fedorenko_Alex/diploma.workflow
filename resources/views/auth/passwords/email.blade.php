@extends('layouts.app')

@section('page')

<nav class="navbar navbar-expand-lg bg-light border-bottom shadow-sm static-top">
        <div class="container">
            <a class="navbar-brand" href="{{url('/')}}">
                <img src="{{ asset('images/logo.png')  }}" class="logo" alt="">
                <div class="aknt_fullname avenir-light text-aknt">@lang('welcome.department_full_name')</div>
            </a>

            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        @if ( Config::get('app.locale') == 'en')
                            <a class="nav-link disabled pr-0" tabindex="-1" aria-disabled="true">EN</a>
                        @else
                            <a class="nav-link pr-0 text-aknt" href="{{url('/setlocale/en')}}">EN</a>
                        @endif
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled pr-0 pl-0" tabindex="-1" aria-disabled="true">/</a>
                    </li>
                    <li class="nav-item">
                        @if ( Config::get('app.locale') == 'uk')
                            <a class="nav-link disabled pl-0" tabindex="-1" aria-disabled="true">UK</a>
                        @else
                            <a class="nav-link pl-0 text-aknt" href="{{url('/setlocale/uk')}}">UK</a>
                        @endif
                    </li>
                </ul>
            </div>
        </div>
    </nav>


<div class="container mb-5">

    <div class="row justify-content-md-center mt-5">
        <div class="col-8">
            <h3 class="text-center" style="font-weight: bold;">@lang('auth.restore_pass')</h3>
        </div>
    </div>

    @if (session('status'))
        <div class="alert alert-success mt-5" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="row justify-content-md-center mt-5">
        <div class="col-6">
            <form method="POST" action="{{ route('password.email') }}">
                @csrf

                <div class="form-group row">
                    <div class="col">
                        <input type="email"
                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                               value="{{ old('email') }}" placeholder="@lang('auth.email')" required>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col d-flex justify-content-center">
                        <button type="submit" class="btn bg-aknt ">
                            @lang('auth.send_restore')
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
