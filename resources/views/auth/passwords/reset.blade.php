@extends('layouts.app')

@section('page')

<nav class="navbar navbar-expand-lg bg-light border-bottom shadow-sm static-top">
        <div class="container">
            <a class="navbar-brand" href="{{url('/')}}">
                <img src="{{ asset('images/logo.png')  }}" class="logo" alt="">
                <div class="aknt_fullname avenir-light text-aknt">@lang('welcome.department_full_name')</div>
            </a>
        </div>
    </nav>

<div class="container mb-5">
    <div class="row justify-content-md-center mt-5">
        <div class="col-8">
            <h3 class="text-center" style="font-weight: bold;">@lang('auth.restore_pass')</h3>
        </div>
    </div>
    <div class="row justify-content-md-center mt-5">
        <div class="col-8">
            <form method="POST" action="{{ route('password.update') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group row">
                    <div class="col">
                        <input type="email"
                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                               value="{{ old('email') }}" placeholder="@lang('auth.email')" required>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col">
                        <input type="password"
                               class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                               placeholder="@lang('auth.password')" name="password" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col">
                        <input type="password" class="form-control"
                               name="password_confirmation" placeholder="@lang('auth.password_confirm')" required>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col d-flex justify-content-center">
                        <button type="submit" class="btn bg-aknt ">
                            @lang('auth.set_new_pass')
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
