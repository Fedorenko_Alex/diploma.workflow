@extends('layouts.app')

@section('page')
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg bg-light border-bottom static-top">
        <div class="container">
            <a class="navbar-brand" href="{{url('/')}}">
                <img src="{{ asset('images/logo.png')  }}" class="logo" alt="">
                <div class="aknt_fullname avenir-thin text-aknt">@lang('welcome.department_full_name')</div>
            </a>

            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        @if ( Config::get('app.locale') == 'en')
                            <a class="nav-link disabled pr-0" tabindex="-1" aria-disabled="true">ENG</a>
                        @else
                            <a class="nav-link pr-0 text-aknt" href="{{url('/setlocale/en')}}">ENG</a>
                        @endif
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled pr-0 pl-0" tabindex="-1" aria-disabled="true">/</a>
                    </li>
                    <li class="nav-item">
                        @if ( Config::get('app.locale') == 'uk')
                            <a class="nav-link disabled pl-0" tabindex="-1" aria-disabled="true">УКР</a>
                        @else
                            <a class="nav-link pl-0 text-aknt" href="{{url('/setlocale/uk')}}">УКР</a>
                        @endif
                    </li>
                    <li class="nav-item log">
                        <a class="nav-link text-aknt switch_welcome_form" href="#">@lang('auth.login')</a>
                    </li>
                    <li class="nav-item reg d-none">
                        <a class="nav-link text-aknt switch_welcome_form" href="#">@lang('auth.register')</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Page Content -->
    <div class="container mb-5">
        <div class="row justify-content-md-center mt-5">
            <div class="col-8">
                <h3 class="text-center">@lang('welcome.header')</h3>
            </div>
        </div>

        @if ($errors->has('alert'))
            <div class="alert alert-warning mt-5 text-center" role="alert">
                <p class="mb-0">@lang('welcome.account_blocked')</p>
            </div>
        @endif

        <div class="row justify-content-md-center mt-5">
            <div class="col">
                <h3 class="text-justify pr-4 avenir-thin">@lang('welcome.description')</h3>
            </div>
            <div class="col">
                <form method="POST" action="{{ route('register') }}" class="reg">
                    @csrf
                    <div class="form-group row">
                        <div class="col">
                            <input type="text"
                                   class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                                   value="{{ old('name') }}" placeholder="@lang('auth.name')" required autofocus>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col">
                            <input type="email"
                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                   value="{{ old('email') }}" placeholder="@lang('auth.email')" required>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col">
                            <input type="password"
                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                   placeholder="@lang('auth.password')" name="password" required>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col">
                            <input type="password" class="form-control"
                                   name="password_confirmation" placeholder="@lang('auth.password_confirm')" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="role_check" name="role"
                                       data-toggle="collapse" data-target="#collapseStudent" aria-expanded="false"
                                       aria-controls="collapseStudent" {{Session::has('role')?' checked':''}}>
                                <label class="custom-control-label" for="role_check">@lang('auth.role')</label>
                            </div>
                        </div>
                    </div>

                    <div class="collapse" id="collapseStudent">
                        <div class="form-group row">
                            <div class="col">
                                <select name="group"
                                        class="custom-select {{ $errors->has('group') ? ' is-invalid' : '' }}">
                                    <option value="" selected disabled hidden>@lang('auth.group')</option>
                                    @foreach($groups as $group)
                                        <option value="{{$group->id}}">{{$group->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('group'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('group') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col">
                            <button type="submit" class="btn bg-aknt w-100">
                                @lang('auth.register')
                            </button>
                            <small class="d-block text-center fs_18">@lang('auth.already_registered')
                                <a href="#" class="text-primary switch_welcome_form">@lang('auth.auth')</a></small>
                        </div>
                    </div>
                </form>
                <form method="POST" action="{{ route('login') }}" class="log d-none">
                    @csrf

                    <div class="form-group row">
                        <div class="col">
                            <input type="email"
                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                   value="{{ old('email') }}" placeholder="@lang('auth.email')" required>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col">
                            <input type="password"
                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                   placeholder="@lang('auth.password')" name="password" required>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col">
                            <button type="submit" class="btn bg-aknt w-100">
                                @lang('auth.login')
                            </button>
                            <small class="d-block text-center fs_18"><a href="{{ url('/password/reset') }}"
                                                                               class="text-primary">@lang('auth.forgot_pass')</a>
                            </small>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container -->

    <section class="pt-5 pb-5 bg-light prosAndCons avenir-thin">
        <div class="container">
            <h2 class="d-block text-center mb-4">@lang('welcome.advantages')</h2>
            <div class="card-columns card-columns_2">

                <div class="card mb-4">
                    <div class="card-body cons">
                        <div class="icon"></div>
                        <span class="info">@lang('welcome.cons_price')</span>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-body cons">
                        <div class="icon"></div>
                        <span class="info">@lang('welcome.cons_time')</span>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-body cons">
                        <div class="icon"></div>
                        <span class="info">@lang('welcome.cons_mess')</span>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-body pros">
                        <div class="icon"></div>
                        <span class="info">@lang('welcome.pros_price')</span>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-body pros">
                        <div class="icon"></div>
                        <span class="info">@lang('welcome.pros_time')</span>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-body pros">
                        <div class="icon"></div>
                        <span class="info">@lang('welcome.pros_mess')</span>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="pt-5 pb-5 bg-transparent avenir-thin">
        <div class="container">
            <h2 class="d-block text-center mb-4">@lang('welcome.features')</h2>
            <ul class="list-group list-group-flush">
                <li class="list-group-item fs_18"><i
                            class="fas fa-paper-plane text-aknt mr-4"></i>@lang('welcome.create_doc')</li>
                <li class="list-group-item fs_18"><i
                            class="fas fa-paper-plane text-aknt mr-4"></i>@lang('welcome.load_doc')</li>
                <li class="list-group-item fs_18"><i
                            class="fas fa-paper-plane text-aknt mr-4"></i>@lang('welcome.send_doc')</li>
                <li class="list-group-item fs_18"><i
                            class="fas fa-paper-plane text-aknt mr-4"></i>@lang('welcome.publish_doc')</li>
                <li class="list-group-item fs_18"><i
                            class="fas fa-paper-plane text-aknt mr-4"></i>@lang('welcome.vision_doc')</li>
            </ul>
        </div>
    </section>

    @if (Session::has('login'))
        <script>
            window.onload = function () {
                $('.switch_welcome_form').click();
            };
        </script>
        {{Session::forget('login')}}
    @endif

    @if(Session::has('role'))
        <script>
            window.onload = function () {
                $('#collapseStudent').collapse('show');
            };
        </script>
        {{Session::forget('role')}}
    @endif
@endsection