@extends('home')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <nav class="aknt_nav d-inline-block hidden-xs-down bg-faded sidebar p-0 mt-2 fs_18">
                <div class="d-flex justify-content-center mb-3 mt-2">
                    <button type="submit" class="btn bg-aknt m-auto" data-toggle="modal"
                            data-target="#upload_file">
                        @lang('aside.upload')
                    </button>
                </div>
                <ul class="nav flex-column aknt_aside">
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/home')}}">@lang('aside.documents')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/income')}}">@lang('aside.income')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/published')}}">@lang('aside.published')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/visions')}}">@lang('aside.vision')</a>
                    </li>
                    @if(Auth::user()->isAdmin())
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('/templates')}}">@lang('aside.templates')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('/docTypes')}}">@lang('aside.doc_types')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('/users')}}">@lang('aside.users')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('/groups')}}">@lang('aside.groups')</a>
                        </li>
                    @endif
                </ul>

            </nav>

            <div class="modal fade" id="upload_file" tabindex="-1" role="dialog"
                 aria-labelledby="edit_0" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">@lang('documents.new_template')</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <ul class="nav nav-tabs nav-fill" role="tablist" id="document_creating_options">
                            <li class="nav-item">
                                <a class="nav-link active" id="from_template-tab" data-toggle="tab"
                                   href="#from_template" role="tab" aria-controls="from_template"
                                   aria-selected="true">@lang('documents.from_template')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom_file-tab" data-toggle="tab" href="#custom_file"
                                   role="tab" aria-controls="custom_file"
                                   aria-selected="false">@lang('documents.custom_file')</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="from_template" role="tabpanel"
                                 aria-labelledby="from_template-tab">
                                <form method="POST" action="{{ url('/create') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="container">
                                        <div class="form-group mt-3">
                                            <select name="type_id" data-url="{{ url('/document_additional') }}"
                                                    class="custom-select {{ ($errors->has('type_id') && Session::has('create')) ? ' is-invalid' : '' }}">
                                                <option value="" selected disabled
                                                        hidden>@lang('documents.choose_template')</option>
                                                @isset(Session::get('file_types')[0])
                                                    @foreach(Session::get('file_types')[0] as $type)
                                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                                    @endforeach
                                                @endisset
                                            </select>
                                            @if (($errors->has('type_id') && Session::has('create')))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('type_id') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label>@lang('documents.comment')</label>
                                            <textarea
                                                    class="form-control{{ ($errors->has('comment') && Session::has('create')) ? ' is-invalid' : '' }}"
                                                    name="comment" rows="3"></textarea>
                                            @if ($errors->has('comment') && Session::has('create') )
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('comment') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <h5>@lang('documents.fields')</h5>
                                        <div id="template_fields"></div>
                                        <h5>@lang('documents.additional_files')</h5>
                                        <div id="template_files"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn bg-aknt">@lang('documents.create')</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="custom_file" role="tabpanel" aria-labelledby="custom_file-tab">
                                <form method="POST" action="{{ url('/upload') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="container">
                                        <div class="form-group mt-3">
                                            <select name="type_id"
                                                    class="custom-select {{ ($errors->has('type_id') && Session::has('/create')) ? ' is-invalid' : '' }}">
                                                <option value="" selected disabled
                                                        hidden>@lang('documents.type')</option>
                                                @isset(Session::get('file_types')[0])
                                                    @foreach(Session::get('file_types')[0] as $type)
                                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                                    @endforeach
                                                @endisset
                                            </select>
                                            @if (($errors->has('type_id') && Session::has('/create')))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('type_id') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="system_name">@lang('documents.system_name')</label>
                                            <input type="text" name="system_name"
                                                   class="form-control{{ ($errors->has('system_name') && Session::has('create')) ? ' is-invalid' : '' }}">
                                            @if ($errors->has('system_name') && Session::has('create'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('system_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <label for="">@lang('documents.choose_file')</label>
                                        <div class="custom-file mb-3">
                                            <input type="file" class="custom-file-input" id="users_file" name="users_file">
                                            <label class="custom-file-label overflow-hidden {{ ($errors->has('') && Session::has('create')) ? ' is-invalid' : '' }}"
                                                   for="users_file" data-browse="@lang('aside.upload')"></label>
                                            @if ($errors->has('') && Session::has('create'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label>@lang('documents.comment')</label>
                                            <textarea
                                                    class="form-control{{ ($errors->has('comment') && Session::has('create')) ? ' is-invalid' : '' }}"
                                                    name="comment" rows="3"></textarea>
                                            @if ($errors->has('comment') && Session::has('create') )
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('comment') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="submit" class="btn bg-aknt">@lang('aside.upload')</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <main class="d-inline-block pt-3">
                @yield('main')
            </main>
        </div>
    </div>
@endsection