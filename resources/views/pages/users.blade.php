@extends('profile')

@section('main')
    <table class="table table-hover table-striped mt-5 text-center">
        <thead>
        <tr>
            <th scope="col">@lang('users.name')</th>
            <th scope="col">@lang('users.group')</th>
            <th scope="col">@lang('users.email')</th>
            <th scope="col">@lang('users.status')</th>
            <th scope="col">@lang('users.appoint')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{$user->name}}</td>
                <td>
                    {{$user->group_name}}
                    @if($user->id == $user->appoint)
                        <span class="text-muted">
                           (@lang(($user->group == 1)?'users.head_dep':'users.head_group'))
                        </span>
                    @endif
                </td>
                <td>{{$user->email}}</td>
                <td>
                    @if($user->role == 'admin')
                        @lang('users.admin')
                    @elseif($user->status)
                        <button class="btn text-success bg-transparent m-auto users-status"
                                data-url="{{ url('/users/block/'.$user->id) }}">
                            @lang('users.block')
                        </button>
                    @else
                        <button class="btn text-warning bg-transparent m-auto users-status"
                                data-url="{{ url('/users/block/'.$user->id) }}">
                            @lang('users.unblock')
                        </button>
                    @endif
                </td>
                <td>
                    @if(is_null($user->appoint))
                        <button type="button" class="btn text-aknt bg-transparent"
                                data-reload="{{url('/users/appoint/'.$user->id)}}">@lang(($user->group == 1)?'users.appoint_head_dep':'users.appoint_head_group')</button>
                    @elseif($user->appoint== $user->id)
                        <button type="button" class="btn text-aknt bg-transparent"
                                data-reload="{{url('/users/appoint/'.$user->id)}}">@lang('users.fire')</button>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    <script>
        window.onload = function () {
            switchAside(7);
        };
    </script>
@endsection