@extends('profile')

@section('main')

    <div class="d-flex justify-content-end">
        <button class="btn bg-aknt mb-3" data-toggle="modal"
                data-target="#edit_0">
            @lang('documents.new_template')
        </button>
    </div>

    <div class="modal fade" id="edit_0" tabindex="-1" role="dialog"
         aria-labelledby="edit_0" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('documents.new_template')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('/templates/edit/0') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="exampleInputEmail1">@lang('groups.name')</label>
                            <input type="text" name="name"
                                   class="form-control{{ ($errors->has('name') && Session::has('template') && Session::get('template')==0) ? ' is-invalid' : '' }}">
                            @if ($errors->has('name') && Session::has('template') && Session::get('template')==0)
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <select name="type_id"
                                    class="custom-select {{ ($errors->has('type_id') && Session::has('template') && Session::get('template')==0) ? ' is-invalid' : '' }}">
                                <option value="" selected disabled hidden>@lang('documents.type')</option>
                                @foreach($types as $type)
                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                @endforeach
                            </select>
                            @if (($errors->has('type_id') && Session::has('template') && Session::get('template')==0))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('type_id') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>@lang('documents.comment')</label>
                            <textarea
                                    class="form-control{{ ($errors->has('comment') && Session::has('template') && Session::get('template')==0) ? ' is-invalid' : '' }}"
                                    name="comment" rows="3"></textarea>
                            @if ($errors->has('comment') && Session::has('template') && Session::get('template')==0)
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('comment') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="custom-control custom-switch mb-3">
                            <input type="checkbox" class="custom-control-input" id="visibility" name="visibility">
                            <label class="custom-control-label" for="visibility">@lang('documents.visibility')</label>
                        </div>

                        <label>@lang('documents.available_for')</label>
                        <div class="w-100 mb-3">
                            <div class="custom-control custom-checkbox d-inline-flex mr-2">
                                <input type="checkbox" class="custom-control-input" id="head_dep" name="available[]"
                                       value="2">
                                <label class="custom-control-label" for="head_dep">@lang('users.head_dep')</label>
                            </div>
                            <div class="custom-control custom-checkbox d-inline-flex mr-2">
                                <input type="checkbox" class="custom-control-input" id="tutor" name="available[]"
                                       value="3">
                                <label class="custom-control-label" for="tutor">@lang('users.tutor')</label>
                            </div>
                            <div class="custom-control custom-checkbox d-inline-flex mr-2">
                                <input type="checkbox" class="custom-control-input" id="head_group" name="available[]"
                                       value="4">
                                <label class="custom-control-label" for="head_group">@lang('users.head_group')</label>
                            </div>
                            <div class="custom-control custom-checkbox d-inline-flex mr-2">
                                <input type="checkbox" class="custom-control-input" id="student" name="available[]"
                                       value="5">
                                <label class="custom-control-label" for="student">@lang('users.student')</label>
                            </div>
                        </div>

                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="template_file" name="template_file">
                            <label class="custom-file-label overflow-hidden {{ ($errors->has('template_file') && Session::has('template') && Session::get('template')==0) ? ' is-invalid' : '' }}"
                                   for="template_file" data-browse="@lang('aside.upload')"></label>
                            @if ($errors->has('template_file') && Session::has('template') && Session::get('template')==0)
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('template_file') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <h5>@lang('documents.fields')</h5>

                        <div class="d-flex justify-content-end">
                            <input type="hidden" name="fields_num">
                            <button class="btn bg-transparent text-aknt add_field"
                                    data-tag="@lang('documents.template_tag')"
                                    data-description="@lang('documents.field_description')">@lang('documents.add_field')</button>
                        </div>

                        <h5>@lang('documents.additional_files')</h5>

                        <div class="d-flex justify-content-end">
                            <input type="hidden" name="files_num">
                            <button class="btn bg-transparent text-aknt add_file"
                                    data-required="@lang('documents.required')"
                                    data-description="@lang('documents.file_description')">@lang('documents.add_file')</button>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn bg-aknt">@lang('groups.save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @if(count($templates))
    <table class="table table-hover table-striped text-center">
        <thead>
        <tr>
            <th scope="col">@lang('groups.name')</th>
            <th scope="col">@lang('documents.type')</th>
            <th scope="col">@lang('documents.visibility')</th>
            <th scope="col">@lang('documents.available_for')</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($templates as $template)
            <tr>
                <td>
                    {{$template->name}}
                    <span class="text-muted">{{$template->comment}}</span>
                </td>
                <td>
                    {{$template->type_name}}
                </td>
                <td>
                    @if($template->visibility)
                        @lang('documents.visible')
                    @else
                        @lang('documents.hidden')
                    @endif
                </td>
                <td>
                    @isset($template->visible_for['2'])
                        @lang('users.head_dep'),
                    @endisset
                    @isset($template->visible_for['3'])
                        @lang('users.tutor'),
                    @endisset
                    @isset($template->visible_for['4'])
                        @lang('users.head_group'),
                    @endisset
                    @isset($template->visible_for['5'])
                        @lang('users.student'),
                    @endisset
                </td>
                <td>
                    <button type="submit" class="btn bg-aknt m-auto" data-toggle="modal"
                            data-target="#edit_{{$template->id}}">
                        @lang('groups.edit')
                    </button>
                </td>
            </tr>

            <div class="modal fade" id="edit_{{$template->id}}" tabindex="-1" role="dialog"
                 aria-labelledby="edit_{{$template->id}}" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">@lang('documents.new_template')</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="POST" action="{{ url('/templates/edit/'.$template->id) }}"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">

                                <div class="form-group">
                                    <label for="exampleInputEmail1">@lang('groups.name')</label>
                                    <input type="text" name="name" value="{{$template->name}}"
                                           class="form-control{{ ($errors->has('name') && Session::has('template') && Session::get('template')==$template->id) ? ' is-invalid' : '' }}">
                                    @if ($errors->has('name') && Session::has('template') && Session::get('template')==$template->id)
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <select name="type_id"
                                            class="custom-select {{ ($errors->has('type_id') && Session::has('template') && Session::get('template')==$template->id) ? ' is-invalid' : '' }}">
                                        <option value="" selected disabled hidden>@lang('documents.type')</option>
                                        @foreach($types as $type)
                                            <option value="{{$type->id}}"
                                                    @if($type->id == $template->type_id)
                                                    selected
                                                    @endif
                                            >{{$type->name}}</option>
                                        @endforeach
                                    </select>
                                    @if (($errors->has('type_id') && Session::has('template') && Session::get('template')==$template->id))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('type_id') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>@lang('documents.comment')</label>
                                    <textarea
                                            class="form-control{{ ($errors->has('comment') && Session::has('template') && Session::get('template')==$template->id) ? ' is-invalid' : '' }}"
                                            name="comment" rows="3">{{$template->comment}}</textarea>
                                    @if ($errors->has('comment') && Session::has('template') && Session::get('template')==$template->id)
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('comment') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="custom-control custom-switch mb-3">
                                    <input type="checkbox" class="custom-control-input" id="visibility"
                                           name="visibility" {{ !($template->visibility)?'':'checked' }} >
                                    <label class="custom-control-label"
                                           for="visibility">@lang('documents.visibility')</label>
                                </div>

                                <label>@lang('documents.available_for')</label>
                                <div class="w-100 mb-3">
                                    <div class="custom-control custom-checkbox d-inline-flex mr-2">
                                        <input type="checkbox" class="custom-control-input" id="head_dep"
                                               name="available[]"
                                               value="2"
                                               @isset($template->visible_for['2'])
                                               checked
                                                @endisset>
                                        <label class="custom-control-label"
                                               for="head_dep">@lang('users.head_dep')</label>
                                    </div>
                                    <div class="custom-control custom-checkbox d-inline-flex mr-2">
                                        <input type="checkbox" class="custom-control-input" id="tutor"
                                               name="available[]"
                                               value="3"
                                               @isset($template->visible_for['3'])
                                               checked
                                                @endisset>
                                        <label class="custom-control-label" for="tutor">@lang('users.tutor')</label>
                                    </div>
                                    <div class="custom-control custom-checkbox d-inline-flex mr-2">
                                        <input type="checkbox" class="custom-control-input" id="head_group"
                                               name="available[]"
                                               value="4"
                                               @isset($template->visible_for['4'])
                                               checked
                                                @endisset>
                                        <label class="custom-control-label"
                                               for="head_group">@lang('users.head_group')</label>
                                    </div>
                                    <div class="custom-control custom-checkbox d-inline-flex mr-2">
                                        <input type="checkbox" class="custom-control-input" id="student"
                                               name="available[]"
                                               value="5"
                                               @isset($template->visible_for['5'])
                                               checked
                                                @endisset>
                                        <label class="custom-control-label" for="student">@lang('users.student')</label>
                                    </div>
                                </div>

                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="template_file"
                                           name="template_file">
                                    <label class="custom-file-label overflow-hidden {{ ($errors->has('template_file') && Session::has('template') && Session::get('template')==$template->id) ? ' is-invalid' : '' }}"
                                           for="template_file"
                                           data-browse="@lang('aside.upload')">{{$template->template_file_path}}</label>
                                    @if ($errors->has('template_file') && Session::has('template') && Session::get('template')==$template->id)
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('template_file') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <h5>@lang('documents.fields')</h5>

                                @foreach($template->attributes as $number => $attribute)

                                    <div class="row mb-3">
                                        <div class="col">
                                            <input type="text" class="form-control"
                                                   name="field_description_{{$number}}"
                                                   value="{{$attribute['description']}}"
                                                   placeholder="@lang('documents.field_description')">
                                        </div>
                                        <div class="col">
                                            <input type="text" class="form-control" name="field_tag_{{$number}}"
                                                   value="{{$attribute['tag']}}"
                                                   placeholder="@lang('documents.template_tag')">
                                        </div>
                                    </div>

                                @endforeach

                                <div class="d-flex justify-content-end">
                                    <input type="hidden" name="fields_num">
                                    <button class="btn bg-transparent text-aknt add_field"
                                            data-tag="@lang('documents.template_tag')"
                                            data-description="@lang('documents.field_description')">@lang('documents.add_field')</button>
                                </div>

                                <h5>@lang('documents.additional_files')</h5>
                                @if(isset($files_sorted[$template->id]) && is_array($files_sorted[$template->id]))
                                    @foreach($files_sorted[$template->id] as $number=>$file_sorted)
                                        <div class="row mb-3">
                                            <div class="col">
                                                <input type="text" class="form-control"
                                                       name="file_description_{{$number}}" value="{{$file_sorted->name}}"
                                                       placeholder="@lang('documents.file_description')">
                                            </div>
                                            <div class="col d-flex align-items-center justify-content-center custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input"
                                                       id="file_required_{{$number}}"
                                                       name="file_required_{{$number}}" {{!($file_sorted->required)?'':' checked'}}>
                                                <label class="custom-control-label" for="file_required_{{$number}}">@lang('documents.required')</label>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                <div class="d-flex justify-content-end">
                                    <input type="hidden" name="files_num">
                                    <button class="btn bg-transparent text-aknt add_file"
                                            data-required="@lang('documents.required')"
                                            data-description="@lang('documents.file_description')">@lang('documents.add_file')</button>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn bg-aknt">@lang('groups.save')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        @endforeach
        </tbody>
    </table>
    @endif
    <script>
        window.onload = function () {
            switchAside(5);
            @if(Session::has('template'))
            $('#edit_{{Session::get('template')}}').modal('show')
            {{Session::forget('template')}}
            @endif
        };
    </script>
@endsection