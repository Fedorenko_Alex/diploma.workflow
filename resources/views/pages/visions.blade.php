@extends('profile')

@section('main')

    <ul class="nav nav-tabs nav-fill" role="tablist">
        <li class="nav-item">
            <a class="nav-link text-aknt active" id="pending-tab" data-toggle="tab"
               href="#pending" role="tab" aria-controls="pending"
               aria-selected="true">@lang('documents.my_pending')</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-aknt" id="income-tab" data-toggle="tab" href="#income"
               role="tab" aria-controls="income"
               aria-selected="false">@lang('aside.income')</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-aknt" id="complete-tab" data-toggle="tab" href="#complete"
               role="tab" aria-controls="complete"
               aria-selected="false">@lang('documents.my_complete')</a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="pending" role="tabpanel" aria-labelledby="pending-tab">
            @if(count($pending))
                <table class="table table-hover table-striped text-center">
                    <thead>
                    <tr>
                        <th scope="col">@lang('documents.name')</th>
                        <th scope="col">@lang('documents.vision_date')</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($pending as $document)
                        <tr>
                            <td><a href="#" data-toggle="modal"
                                   data-target="#document_id_{{$document->id}}"
                                   class="text-aknt">{{$document->doc_name}}</a>
                                <span class="text-muted">{{$document->comment}}</span>
                            </td>
                            <td>
                                {{$document->created_at}}
                            </td>
                            <td>
                                <button class="btn text-aknt bg-transparent m-auto" data-toggle="modal"
                                        data-target="#route_{{$document->vision_id}}">
                                    @lang('documents.visas')
                                </button>
                            </td>
                        </tr>

                        <div class="modal fade" id="document_id_{{$document->id}}" tabindex="-1" role="dialog"
                             aria-labelledby="document_id_{{$document->id}}" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">@lang('documents.info')</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <dl class="row">

                                            <dt class="col-sm-4">@lang('documents.system_name')</dt>
                                            <dd class="col-sm-8">{{$document->doc_name}}</dd>

                                            <dt class="col-sm-4">@lang('documents.type')</dt>
                                            <dd class="col-sm-8">{{$document->type_name}}</dd>

                                            <dt class="col-sm-4">@lang('documents.comment')</dt>
                                            <dd class="col-sm-8">{{$document->comment}}</dd>

                                            <dt class="col-sm-4">@lang('documents.upload_date')</dt>
                                            <dd class="col-sm-8">{{$document->created_at}}</dd>

                                            <dt class="col-sm-4"></dt>
                                            <dd class="col-sm-8"><a class="text-aknt bg-transparent"
                                                                    href="{{url('download/'.$document->id)}}">@lang('documents.download')</a>
                                            </dd>
                                        </dl>
                                        @if(count($document->additional))
                                            <h5>@lang('documents.additional_files')</h5>
                                            <dl class="row">
                                                @foreach($document->additional as $additional)
                                                    <dt class="col-sm-4">{{$additional->name}}</dt>
                                                    <dd class="col-sm-8"><a class="text-aknt bg-transparent"
                                                                            href="{{url('download/additional/'.$additional->id)}}">@lang('documents.download')</a>
                                                    </dd>
                                                @endforeach
                                            </dl>
                                        @endif


                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="route_{{$document->vision_id}}" tabindex="-1" role="dialog"
                             aria-labelledby="route_{{$document->vision_id}}" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">@lang('documents.route')</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <ul class="list-group list-group-flush">
                                            @foreach($document->route as $viewer_id=>$route)
                                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                                    {{$route['name']}}
                                                    @if($document->status == 0 && $viewer_id == $document->next_viewer)
                                                        <span class="text-danger">@lang('documents.rejected')</span>
                                                    @elseif($route['date']>0)
                                                        <span>{{$route['date']}}</span>
                                                    @else
                                                        <span class="text-muted">__.__.____ --:--:--</span>
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
        <div class="tab-pane" id="income" role="tabpanel" aria-labelledby="income-tab">
            @if(count($income))
                <table class="table table-hover table-striped text-center">
                    <thead>
                    <tr>
                        <th scope="col">@lang('documents.owner')</th>
                        <th scope="col">@lang('documents.name')</th>
                        <th scope="col">@lang('documents.vision_date')</th>
                        <th scope="col" colspan="3"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($income as $document)
                        <tr>
                            <td>{{$document->sender}}</td>
                            <td><a href="#" data-toggle="modal"
                                   data-target="#document_id_{{$document->id}}"
                                   class="text-aknt">{{$document->doc_name}}</a>
                                <span class="text-muted">{{$document->comment}}</span>
                            </td>
                            <td>
                                {{$document->created_at}}
                            </td>
                            <td>
                                <button class="btn text-aknt bg-transparent m-auto" data-toggle="modal"
                                        data-target="#route_{{$document->vision_id}}">
                                    @lang('documents.visas')
                                </button>
                            </td>
                            <td>
                                <button class="btn text-success bg-transparent m-auto vision_action"
                                        data-url="{{url('/vision/approve/'.$document->vision_id)}}">
                                    @lang('documents.approve')
                                </button>
                            </td>
                            <td>
                                <button class="btn text-danger bg-transparent m-auto vision_action"
                                        data-url="{{url('/vision/reject/'.$document->vision_id)}}">
                                    @lang('documents.reject')
                                </button>
                            </td>
                        </tr>

                        <div class="modal fade" id="document_id_{{$document->id}}" tabindex="-1" role="dialog"
                             aria-labelledby="document_id_{{$document->id}}" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">@lang('documents.info')</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <dl class="row">
                                            <dt class="col-sm-4">@lang('documents.owner')</dt>
                                            <dd class="col-sm-8">{{$document->sender}}</dd>

                                            <dt class="col-sm-4">@lang('documents.system_name')</dt>
                                            <dd class="col-sm-8">{{$document->doc_name}}</dd>

                                            <dt class="col-sm-4">@lang('documents.type')</dt>
                                            <dd class="col-sm-8">{{$document->type_name}}</dd>

                                            <dt class="col-sm-4">@lang('documents.comment')</dt>
                                            <dd class="col-sm-8">{{$document->comment}}</dd>

                                            <dt class="col-sm-4">@lang('documents.upload_date')</dt>
                                            <dd class="col-sm-8">{{$document->created_at}}</dd>

                                            <dt class="col-sm-4"></dt>
                                            <dd class="col-sm-8"><a class="text-aknt bg-transparent"
                                                                    href="{{url('download/'.$document->id)}}">@lang('documents.download')</a>
                                            </dd>
                                        </dl>
                                        @if(count($document->additional))
                                            <h5>@lang('documents.additional_files')</h5>
                                            <dl class="row">
                                                @foreach($document->additional as $additional)
                                                    <dt class="col-sm-4">{{$additional->name}}</dt>
                                                    <dd class="col-sm-8"><a class="text-aknt bg-transparent"
                                                                            href="{{url('download/additional/'.$additional->id)}}">@lang('documents.download')</a>
                                                    </dd>
                                                @endforeach
                                            </dl>
                                        @endif


                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="route_{{$document->vision_id}}" tabindex="-1" role="dialog"
                             aria-labelledby="route_{{$document->vision_id}}" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">@lang('documents.route')</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <ul class="list-group list-group-flush">
                                            @foreach($document->route as $viewer_id=>$route)
                                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                                    {{$route['name']}}
                                                    @if($document->status == 0 && $viewer_id == $document->next_viewer)
                                                        <span class="text-danger">@lang('documents.rejected')</span>
                                                    @elseif($route['date']>0)
                                                        <span>{{$route['date']}}</span>
                                                    @else
                                                        <span class="text-muted">__.__.____ --:--:--</span>
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
        <div class="tab-pane" id="complete" role="tabpanel" aria-labelledby="complete-tab">
            @if(count($complete))
                <table class="table table-hover table-striped text-center">
                    <thead>
                    <tr>
                        <th scope="col">@lang('documents.name')</th>
                        <th scope="col">@lang('documents.vision_date')</th>
                        <th scope="col">@lang('users.status')</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($complete as $document)
                        <tr>
                            <td><a href="#" data-toggle="modal"
                                   data-target="#document_id_{{$document->id}}"
                                   class="text-aknt">{{$document->doc_name}}</a>
                                <span class="text-muted">{{$document->comment}}</span>
                            </td>
                            <td>
                                {{$document->created_at}}
                            </td>
                            <td>
                                @if($document->status == 10)
                                    <span class="text-success">@lang('documents.approved')</span>
                                @elseif($document->status == 0)
                                    <span class="text-danger">@lang('documents.rejected')</span>
                                @endif
                            </td>
                            <td>
                                <button class="btn text-aknt bg-transparent m-auto" data-toggle="modal"
                                        data-target="#route_{{$document->vision_id}}">
                                    @lang('documents.visas')
                                </button>
                            </td>
                        </tr>

                        <div class="modal fade" id="document_id_{{$document->id}}" tabindex="-1" role="dialog"
                             aria-labelledby="document_id_{{$document->id}}" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">@lang('documents.info')</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <dl class="row">

                                            <dt class="col-sm-4">@lang('documents.system_name')</dt>
                                            <dd class="col-sm-8">{{$document->doc_name}}</dd>

                                            <dt class="col-sm-4">@lang('documents.type')</dt>
                                            <dd class="col-sm-8">{{$document->type_name}}</dd>

                                            <dt class="col-sm-4">@lang('documents.comment')</dt>
                                            <dd class="col-sm-8">{{$document->comment}}</dd>

                                            <dt class="col-sm-4">@lang('documents.upload_date')</dt>
                                            <dd class="col-sm-8">{{$document->created_at}}</dd>

                                            <dt class="col-sm-4"></dt>
                                            <dd class="col-sm-8"><a class="text-aknt bg-transparent"
                                                                    href="{{url('download/'.$document->id)}}">@lang('documents.download')</a>
                                            </dd>
                                        </dl>
                                        @if(count($document->additional))
                                            <h5>@lang('documents.additional_files')</h5>
                                            <dl class="row">
                                                @foreach($document->additional as $additional)
                                                    <dt class="col-sm-4">{{$additional->name}}</dt>
                                                    <dd class="col-sm-8"><a class="text-aknt bg-transparent"
                                                                            href="{{url('download/additional/'.$additional->id)}}">@lang('documents.download')</a>
                                                    </dd>
                                                @endforeach
                                            </dl>
                                        @endif


                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="route_{{$document->vision_id}}" tabindex="-1" role="dialog"
                             aria-labelledby="route_{{$document->vision_id}}" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">@lang('documents.route')</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <ul class="list-group list-group-flush">
                                            @foreach($document->route as $viewer_id=>$route)
                                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                                    {{$route['name']}}
                                                        @if($document->status == 0 && $viewer_id == $document->next_viewer)
                                                        <span class="text-danger">@lang('documents.rejected')</span>
                                                        @elseif($route['date']>0)
                                                        <span>{{$route['date']}}</span>
                                                        @else
                                                        <span class="text-muted">__.__.____ --:--:--</span>
                                                        @endif
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

    <script>
        window.onload = function () {
            switchAside(4);
        };
    </script>

@endsection