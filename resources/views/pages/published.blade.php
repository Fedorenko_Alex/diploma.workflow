@extends('profile')

@section('main')
    @if(count($published))
        <table class="table table-hover table-striped text-center">
            <thead>
            <tr>
                <th scope="col">@lang('documents.owner')</th>
                <th scope="col">@lang('documents.name')</th>
                <th scope="col">@lang('documents.publishing_date')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($published as $document)
                <tr>
                    <td>{{$document->sender}}</td>
                    <td><a href="#" data-toggle="modal"
                           data-target="#document_id_{{$document->id}}" class="text-aknt">{{$document->doc_name}}</a>
                        <span class="text-muted">{{$document->comment}}</span>
                    </td>
                    <td>
                        {{$document->created_at}}
                    </td>
                </tr>

                <div class="modal fade" id="document_id_{{$document->id}}" tabindex="-1" role="dialog"
                     aria-labelledby="document_id_{{$document->id}}" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">@lang('documents.info')</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <dl class="row">
                                    <dt class="col-sm-4">@lang('documents.owner')</dt>
                                    <dd class="col-sm-8">{{$document->sender}}</dd>

                                    <dt class="col-sm-4">@lang('documents.system_name')</dt>
                                    <dd class="col-sm-8">{{$document->doc_name}}</dd>

                                    <dt class="col-sm-4">@lang('documents.type')</dt>
                                    <dd class="col-sm-8">{{$document->type_name}}</dd>

                                    <dt class="col-sm-4">@lang('documents.comment')</dt>
                                    <dd class="col-sm-8">{{$document->comment}}</dd>

                                    <dt class="col-sm-4">@lang('documents.upload_date')</dt>
                                    <dd class="col-sm-8">{{$document->created_at}}</dd>

                                    <dt class="col-sm-4"></dt>
                                    <dd class="col-sm-8"><a class="text-aknt bg-transparent"
                                                            href="{{url('download/'.$document->id)}}">@lang('documents.download')</a>
                                    </dd>
                                </dl>
                                @if(count($document->additional))
                                    <h5>@lang('documents.additional_files')</h5>
                                    <dl class="row">
                                        @foreach($document->additional as $additional)
                                            <dt class="col-sm-4">{{$additional->name}}</dt>
                                            <dd class="col-sm-8"><a class="text-aknt bg-transparent"
                                                                    href="{{url('download/additional/'.$additional->id)}}">@lang('documents.download')</a>
                                            </dd>
                                        @endforeach
                                    </dl>
                                @endif


                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            </tbody>
        </table>
    @endif


    <script>
        window.onload = function () {
            switchAside(3);
        };
    </script>

@endsection