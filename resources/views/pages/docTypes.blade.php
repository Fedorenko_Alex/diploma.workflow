@extends('profile')

@section('main')
    <div class="d-flex justify-content-end">
        <button class="btn bg-aknt mb-3" data-toggle="modal"
                data-target="#edit_0">
            @lang('documents.new_type')
        </button>
    </div>
    <div class="modal fade" id="edit_0" tabindex="-1" role="dialog"
         aria-labelledby="edit_0" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('documents.new_type')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('/docTypes/edit/0') }}">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">@lang('groups.name')</label>
                            <input type="text" name="name"
                                   class="form-control{{ ($errors->has('name') && Session::has('doc_type') && Session::get('doc_type')==0) ? ' is-invalid' : '' }}">
                            @if ($errors->has('name') && Session::has('doc_type') && Session::get('doc_type')==0)
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn bg-aknt">@lang('groups.save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <table class="table table-hover table-striped text-center">
        <thead>
        <tr>
            <th scope="col" colspan="2">@lang('groups.name')</th>
            <th scope="col">@lang('documents.types_templates')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($doc_types as $doc_type)
            <tr>
                <td>{{$doc_type->name}}</td>
                <td>
                    <button type="submit" class="btn text-aknt bg-transparent m-auto" data-toggle="modal"
                            data-target="#edit_{{$doc_type->id}}">
                        @lang('groups.edit')
                    </button>
                </td>
                <td>
                    <button type="submit" class="btn text-aknt bg-transparent m-auto" data-toggle="modal"
                            data-target="#docs_{{$doc_type->id}}">
                        @lang('groups.view')
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @foreach($doc_types as $doc_type)
        <div class="modal fade" id="edit_{{$doc_type->id}}" tabindex="-1" role="dialog"
             aria-labelledby="edit_{{$doc_type->id}}" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@lang('documents.types_edit')</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="{{ url('/docTypes/edit/'.$doc_type->id) }}">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">@lang('groups.name')</label>
                                <input type="text" name="name"
                                       class="form-control{{ ($errors->has('name') && Session::has('doc_type') && Session::get('doc_type')==$group->id) ? ' is-invalid' : '' }}"
                                       value="{{$doc_type->name}}">
                                @if ($errors->has('name') && Session::has('doc_type') && Session::get('doc_type')==$doc_type->id)
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="modal-footer d-flex justify-content-between">
                            @if(!isset($users_sorted[$doc_type->id]))
                                <button type="button" class="btn btn-danger"
                                        data-reload="{{url('/docTypes/delete/'.$doc_type->id)}}">@lang('groups.delete')</button>
                            @endif
                            <button type="submit" class="btn bg-aknt">@lang('groups.save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="docs_{{$doc_type->id}}" tabindex="-1" role="dialog"
             aria-labelledby="docs_{{$doc_type->id}}" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{$doc_type->name}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            @if(isset($docs_sorted[$doc_type->id]) && is_array($docs_sorted[$doc_type->id]))
                                @foreach($docs_sorted[$doc_type->id] as $doc_sorted)
                                    <tr>
                                        <td>{{$doc_sorted->name}}</td>
                                        <td class="text-muted">{{$doc_sorted->comment}}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <script>
        window.onload = function () {
            switchAside(6);
            @if(Session::has('doc_type'))
            $('#edit_{{Session::get('doc_type')}}').modal('show')
            {{Session::forget('doc_type')}}
            @endif
        };
    </script>
@endsection