@extends('profile')

@section('main')
    @if(count($documents))
        <table class="table table-hover table-striped text-center">
            <thead>
            <tr>
                <th scope="col">@lang('documents.system_name')</th>
                <th scope="col" colspan="3"></th>
            </tr>
            </thead>
            <tbody>

            @foreach($documents as $document)
                <tr>
                    <td><a href="#" data-toggle="modal"
                           data-target="#document_id_{{$document->id}}" class="text-aknt">{{$document->name}}</a>
                    </td>
                    <td>
                        <button type="submit" class="btn text-aknt bg-transparent m-auto" data-toggle="modal"
                                data-target="#send_{{$document->id}}">
                            @lang('documents.send')
                        </button>
                    </td>
                    <td>
                        <button type="submit" class="btn text-aknt bg-transparent m-auto" data-toggle="modal"
                                data-target="#publish_{{$document->id}}">
                            @lang('documents.publish')
                        </button>
                    </td>
                    <td>
                        <button type="submit" class="btn text-aknt bg-transparent m-auto" data-toggle="modal"
                                data-target="#vision_{{$document->id}}">
                            @lang('documents.vision')
                        </button>
                    </td>
                </tr>

                <div class="modal fade" id="send_{{$document->id}}" tabindex="-1" role="dialog"
                     aria-labelledby="send_{{$document->id}}" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">@lang('documents.sending')</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ url('/send/'.$document->id) }}">
                                @csrf
                                <div class="modal-body">

                                    <div class="form-group">
                                        <select name="user_id"
                                                class="custom-select {{ ($errors->has('user_id') && Session::has('send')&& Session::get('send') == $document->id) ? ' is-invalid' : '' }}">
                                            <option value="" selected disabled
                                                    hidden>@lang('users.choose')</option>
                                            @if($users)
                                                @foreach($users as $user)
                                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if ($errors->has('name') && Session::has('send') && Session::get('send')==$document->id)
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label>@lang('documents.comment')</label>
                                        <textarea
                                                class="form-control{{ ($errors->has('comment') && Session::has('send')&& Session::get('send') == $document->id) ? ' is-invalid' : '' }}"
                                                name="comment" rows="3"></textarea>
                                        @if ($errors->has('comment') && Session::has('send')&& Session::get('send') == $document->id)
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('comment') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>
                                <div class="modal-footer d-flex justify-content-between">
                                    <button type="submit" class="btn bg-aknt">@lang('documents.send')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="publish_{{$document->id}}" tabindex="-1" role="dialog"
                     aria-labelledby="publish_{{$document->id}}" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">@lang('documents.publishing')</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ url('/publish/'.$document->id) }}">
                                @csrf
                                <div class="modal-body">

                                    <div class="form-group">
                                        <label>@lang('documents.publish_for')</label>
                                        @foreach($groups as $group)
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="groups[]"
                                                       value="{{$group->id}}"
                                                       id="group_{{$document->id}}_{{$group->id}}">
                                                <label class="custom-control-label"
                                                       for="group_{{$document->id}}_{{$group->id}}">{{$group->name}}</label>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="form-group">
                                        <label>@lang('documents.comment')</label>
                                        <textarea
                                                class="form-control{{ ($errors->has('comment') && Session::has('publish')&& Session::get('publish') == $document->id) ? ' is-invalid' : '' }}"
                                                name="comment" rows="3"></textarea>
                                        @if ($errors->has('comment') && Session::has('publish')&& Session::get('publish') == $document->id)
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('comment') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>
                                <div class="modal-footer d-flex justify-content-between">
                                    <button type="submit" class="btn bg-aknt">@lang('documents.publish')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="vision_{{$document->id}}" tabindex="-1" role="dialog"
                     aria-labelledby="vision_{{$document->id}}" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">@lang('documents.vision')</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ url('/vision/'.$document->id) }}"
                                  id="vision_form_{{$document->id}}">
                                @csrf
                                <div class="modal-body">
                                    <label>@lang('documents.form_vision_queue')</label>
                                    <div class="form-group">
                                        <select name="user_queue_0"
                                                class="custom-select" form="vision_form_{{$document->id}}">
                                            <option value="" selected disabled
                                                    hidden>@lang('users.choose')</option>
                                            @if($users)
                                                @foreach($users as $user)
                                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="d-flex justify-content-end">
                                        <input type="hidden" name="queue_length" value="0">
                                        <button class="btn bg-transparent text-aknt add_queue">@lang('documents.add_vision')</button>
                                    </div>

                                    <div class="form-group">
                                        <label>@lang('documents.comment')</label>
                                        <textarea
                                                class="form-control{{ ($errors->has('comment') && Session::has('publish')&& Session::get('publish') == $document->id) ? ' is-invalid' : '' }}"
                                                name="comment" rows="3"></textarea>
                                        @if ($errors->has('comment') && Session::has('publish')&& Session::get('publish') == $document->id)
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('comment') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>
                                <div class="modal-footer d-flex justify-content-between">
                                    <button type="submit" class="btn bg-aknt">@lang('documents.publish')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="document_id_{{$document->id}}" tabindex="-1" role="dialog"
                     aria-labelledby="document_id_{{$document->id}}" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">@lang('documents.info')</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <dl class="row">
                                    {{--<dt class="col-sm-4">@lang('documents.owner')</dt>
                                    <dd class="col-sm-8"></dd>--}}

                                    <dt class="col-sm-4">@lang('documents.system_name')</dt>
                                    <dd class="col-sm-8">{{$document->name}}</dd>

                                    <dt class="col-sm-4">@lang('documents.type')</dt>
                                    <dd class="col-sm-8">{{$document->type_name}}</dd>

                                    <dt class="col-sm-4">@lang('documents.comment')</dt>
                                    <dd class="col-sm-8">{{$document->comment}}</dd>

                                    <dt class="col-sm-4">@lang('documents.upload_date')</dt>
                                    <dd class="col-sm-8">{{$document->created_at}}</dd>

                                    <dt class="col-sm-4"></dt>
                                    <dd class="col-sm-8"><a class="text-aknt bg-transparent"
                                                            href="{{url('download/'.$document->id)}}">@lang('documents.download')</a>
                                    </dd>
                                </dl>
                                @if(count($document->additional))
                                    <h5>@lang('documents.additional_files')</h5>
                                    <dl class="row">
                                        @foreach($document->additional as $additional)
                                            <dt class="col-sm-4">{{$additional->name}}</dt>
                                            <dd class="col-sm-8"><a class="text-aknt bg-transparent"
                                                                    href="{{url('download/additional/'.$additional->id)}}">@lang('documents.download')</a>
                                            </dd>
                                        @endforeach
                                    </dl>
                                @endif


                            </div>
                        </div>
                    </div>
                </div>

            @endforeach
            </tbody>
        </table>
    @endif

    <script>
        window.onload = function () {
            switchAside(1);
            @if(Session::has('upload'))
            $('#upload_file').modal('show');
            $('#document_creating_options li:nth-child({{Session::get('upload')}}) a').tab('show');
            {{Session::forget('upload')}}
            @endif

            @if(Session::has('modal'))
            $('#{{Session::get("modal")}}').modal('show');
            {{Session::forget('modal')}}
            @endif
        };
    </script>

@endsection