@extends('profile')

@section('main')
    <div class="d-flex justify-content-end">
        <button class="btn bg-aknt mb-3" data-toggle="modal"
                data-target="#edit_0">
            @lang('groups.add')
        </button>
    </div>
    <div class="modal fade" id="edit_0" tabindex="-1" role="dialog"
         aria-labelledby="edit_0" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('groups.add')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('/groups/edit/0') }}">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">@lang('groups.name')</label>
                            <input type="text" name="name"
                                   class="form-control{{ ($errors->has('name') && Session::has('group') && Session::get('group')==0) ? ' is-invalid' : '' }}">
                            @if ($errors->has('name') && Session::has('group') && Session::get('group')==0)
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn bg-aknt">@lang('groups.save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <table class="table table-hover table-striped text-center">
        <thead>
        <tr>
            <th scope="col" colspan="2">@lang('groups.name')</th>
            <th scope="col">@lang('groups.users')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($groups as $group)
            <tr>
                <td>{{$group->name}}</td>
                <td>
                    <button type="submit" class="btn text-aknt bg-transparent m-auto" data-toggle="modal"
                            data-target="#edit_{{$group->id}}">
                        @lang('groups.edit')
                    </button>
                </td>
                <td>
                    <button type="submit" class="btn text-aknt bg-transparent m-auto" data-toggle="modal"
                            data-target="#users_{{$group->id}}">
                        @lang('groups.view')
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @foreach($groups as $group)
        <div class="modal fade" id="edit_{{$group->id}}" tabindex="-1" role="dialog"
             aria-labelledby="edit_{{$group->id}}" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@lang('groups.editing')</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="{{ url('/groups/edit/'.$group->id) }}">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">@lang('groups.name')</label>
                                <input type="text" name="name"
                                       class="form-control{{ ($errors->has('name') && Session::has('group') && Session::get('group')==$group->id) ? ' is-invalid' : '' }}"
                                       value="{{$group->name}}">
                                @if ($errors->has('name') && Session::has('group') && Session::get('group')==$group->id)
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="modal-footer d-flex justify-content-between">
                            @if(!isset($users_sorted[$group->id]))
                                <button type="button" class="btn btn-danger"
                                        data-reload="{{url('/groups/delete/'.$group->id)}}">@lang('groups.delete')</button>
                            @endif
                            <button type="submit" class="btn bg-aknt">@lang('groups.save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="users_{{$group->id}}" tabindex="-1" role="dialog"
             aria-labelledby="users_{{$group->id}}" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{$group->name}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            @if(isset($users_sorted[$group->id]) && is_array($users_sorted[$group->id]))
                                @foreach($users_sorted[$group->id] as $user)
                                    <tr>
                                        <td>{{$user->name}}</td>
                                        <td class="text-muted">{{$user->email}}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <script>
        window.onload = function () {
            switchAside(8);
            @if(Session::has('group'))
            $('#edit_{{Session::get('group')}}').modal('show')
            {{Session::forget('group')}}
            @endif
        };
    </script>
@endsection