@extends('layouts.app')

@section('page')

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg bg-light border-bottom static-top">
        <a class="navbar-brand" href="{{url('/')}}">
            <img src="{{ asset('images/logo.png')  }}" class="logo" alt="">
            <div class="aknt_fullname avenir-thin text-aknt">@lang('welcome.department_full_name')</div>
        </a>

        <div class="collapse navbar-collapse" id="navbarResponsive">

            <ul class="navbar-nav ml-auto">
                <li class="nav-item text-aknt">{{Auth::user()->name}}
                        <small class="d-block text-center text-body">{{Auth::user()->GroupName}}</small>
                </li>
            </ul>

            <ul class="navbar-nav ml-3">
                <li class="nav-item">
                    @if ( Config::get('app.locale') == 'en')
                        <a class="nav-link disabled pr-0" tabindex="-1" aria-disabled="true">ENG</a>
                    @else
                        <a class="nav-link pr-0 text-aknt" href="{{url('/setlocale/en')}}">ENG</a>
                    @endif
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled pr-0 pl-0" tabindex="-1" aria-disabled="true">/</a>
                 </li>
                <li class="nav-item">
                    @if ( Config::get('app.locale') == 'uk')
                        <a class="nav-link disabled pl-0" tabindex="-1" aria-disabled="true">УКР</a>
                    @else
                        <a class="nav-link pl-0 text-aknt" href="{{url('/setlocale/uk')}}">УКР</a>
                    @endif
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right ml-4 mr-1">
                <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="text-aknt"><span
                                class="glyphicon glyphicon-log-in"></span> @lang('auth.logout')</a></li>
            </ul>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </nav>

   @yield('content')

@endsection
