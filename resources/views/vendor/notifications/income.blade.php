<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;margin:0;padding:0;">

    <tbody>

    <tr>
        <td height="80"></td>
    </tr>
    <tr>
        <td align="center">

            <!--[if mso | IE]>
            <table border="0" cellpadding="0" cellspacing="0" width="640" align="center" style="width:640px;">

                <tr>

                    <td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">

            <![endif]-->

            <div style="margin:0 auto;max-width:640px;background:#ffffff;">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#f8f8f8;border-radius:5px;overflow: hidden;border-collapse:collapse;margin:0;padding:0;">

                    <tbody>

                    <tr>
                        <td style="padding:10px 15px;margin:0;background-color:#00a8cb;">

                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;margin:0;padding:0;">
                                <tbody>
                                <tr>
                                    <td width="120"><a href="http://aknt.knu.edu.ua" style="padding:0;margin:0;display:block;" rel="nofollow" target="_blank"><img src="http://aknt.knu.edu.ua/img/KNU-LOGO1.png" alt="ACST" title="ACST" width="50" height="50" style="display:inline-block;margin:0;padding:0;"></a></td>
                                    <td><h1 style="margin:0;padding:0;font-size:20px;font-weight:400;color:#ffffff;">Отримано новий документ</h1></td>
                                </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>

                    <tr>
                        <td style="padding:5px 15px;margin:0;">

                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;margin:0;padding:0;">
                                <tbody>

                                <tr>
                                    <td style="padding:10px 0;"><h3 style="padding:0;margin:0;font-size:18px;color:#00a8cb;">Шановний(а) {{$name}}.</h3></td>
                                </tr>
                                <tr>
                                    <td style="padding:0 0 10px;"><p style="padding:0;margin:0;font-size:14px;color:#495057;">Вам було надіслано документ {{$doc_name}} від користувача {{$sender_name}}.</p></td>
                                </tr>
                                <tr>
                                    <td style="padding:0 0 10px;"><p style="padding:0;margin:0;font-size:14px;color:#495057;">Переглянути його можна в своєму особистому кабінеті документообігу кафедри АКНТ.</p></td>
                                </tr>
                                <tr>
                                    <td style="padding:0 0 10px;"><p style="padding:0;margin:0;font-size:14px;color:#495057;"><a href="{{url('/')}}">Перейти</a></p></td>
                                </tr>
                                <tr>
                                    <td style="padding:0 0 10px;"><p style="padding:0;margin:0;font-size:14px;color:#495057;"><i>С найкращими побажаннями, кафедра АНКТ</i></i>.</p></td>
                                </tr>
                                </tbody>
                            </table>

                        </td>
                    </tr>

                    </tbody>

                </table>

            </div>

            <!--[if mso | IE]>

            </td>

            </tr>

            </table>
            <![endif]-->

        </td>

    </tr>

    </tbody>

</table>