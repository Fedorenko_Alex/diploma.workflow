<?php
return [
    'name' => 'Name',
    'save' => 'Save changes',
    'delete' => 'Delete',
    'users' => 'Users, belonging to',
    'view' => 'View',
    'edit' => 'Edit',
    'editing' => 'Group information editing',
    'add' => 'New group',
];