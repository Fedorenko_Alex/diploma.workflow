<?php
return [
    'documents' => 'Documents',
    'groups' => 'Groups',
    'users' => 'Users',
    'templates' => 'Templates',
    'doc_types' => 'Document types',
    'upload' => 'Upload',
    'create' => 'Create using template',
    'income' => 'Income',
    'published' => 'Published',
    'vision' => 'Vision',

];