<?php
return [
    'header'=>'ACST WORKFLOW - it is a service for students and teachers for the mutual exchange of documents in electronic form',
    'description'=>'Now you can easily create standard documents using templates, upload your documents and share them with other system participants in an electronic, not paper-based form.',
    'department_full_name' =>'Department of Automation, <br>computer science and<br>technologies',
    'advantages'=>'Why ACST workflow is better than paperwork?',
    'cons_price'=>'You spend money on paper',
    'cons_time'=>'You spend a lot of time for the design and transfer of documents',
    'cons_mess'=>'Disorder! Documentation is stored in infinite folders and cabinets',
    'pros_price'=>'You create and send documents as easily as e-mail',
    'pros_time'=>'Documents are created and sent in seconds',
    'pros_mess'=>'Documents are structured and stored in a safe place',
    'features'=>'Available functional',
    'create_doc'=>'Create a document by template in the system',
    'load_doc'=>'Load an arbitrary document',
    'send_doc'=>'Sending a document to another user on the system',
    'publish_doc'=>'Publication of a document for certain groups of users',
    'vision_doc'=>'Sending a document for sighting bu a user queue',
    'account_blocked'=>'Your account has been blocked. Contact the system administrator to find out the reasons for the lock',

];