<?php
return [
    'name' => 'Name',
    'group' => 'Group',
    'email' => 'E-mail',
    'status' => 'Status',
    'appoint'=>'To appoint',
    'block'=>'Block',
    'unblock'=>'Unblock',
    'admin'=>'Administrator',
    'head_dep'=>'Head of department',
    'head_group'=>'Head of department',
    'appoint_head_dep'=>'Head of department',
    'appoint_head_group'=>'Head of department',
    'fire'=>'Remove from office',
    'tutor'=>'Tutor',
    'student'=>'Student',
    'choose'=>'Choose user',
];