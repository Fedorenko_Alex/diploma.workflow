<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login'=>'Login',
    'auth'=>'Auth',
    'register'=>'Registration',
    'name'=>'Surname anf name',
    'email'=>'Email',
    'password'=>'Password',
    'password_confirm'=>'Repeat password',
    'role'=>'Student',
    'group'=>'Group',
    'ticket_number'=>'Student ticket series and number',
    'already_registered'=>'Already registered?',
    'forgot_pass'=>'Forgot password?',
    'restore_pass'=>'Reset password',
    'send_restore'=>'Send password recovery link',
    'set_new_pass'=>'Set new password',
    'logout'=>'Logout',
    'verification_header'=>'Your e-mail address requires confirmation',
    'verification_main'=>'To unlock the ACST workflow functionality, you need to use the link that was sent to the e-mail address you specified.',
    'verification_footer'=>'In case you have not received the letter, ',
    'verification_footer_link'=>'use this link to get a new letter.',
    'verification_sent'=>'The reference for verification of the profile has been sent to you by e-mail',


];
