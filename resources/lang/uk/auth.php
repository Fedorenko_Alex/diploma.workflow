<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Облікового запису з вказаними даними не знайдено.',
    'throttle' => 'Забагато невдалих спроб входу. Повторіть спробу за :seconds секунд.',
    'login'=>'Вхід',
    'auth'=>'Авторизуватись',
    'register'=>'Реєстрація',
    'name'=>'Прізвище та ім\'я',
    'email'=>'Email',
    'password'=>'Пароль',
    'password_confirm'=>'Повторити пароль',
    'role'=>'Студент',
    'group'=>'Група',
    'ticket_number'=>'Серія та номер студентського квитка',
    'already_registered'=>'Вже з нами?',
    'forgot_pass'=>'Забули пароль?',
    'restore_pass'=>'Відновлення паролю',
    'send_restore'=>'Відправити посилання для відновлення паролю',
    'set_new_pass'=>'Встановити новий пароль',
    'logout'=>'Вихід',
    'verification_header'=>'Ваша e-mail адреса вимагає підтвердження',
    'verification_main'=>'Для розблокування функціоналу ресурсу АКНТ документообіг необхідно скористатись посиланням, що було відправлено на вказану Вами e-mail адресу.',
    'verification_footer'=>'У разі, якщо Ви не отримали листа, ',
    'verification_footer_link'=>'скористайтеся цим посиланням для отримання нового листа.',
    'verification_sent'=>'Посилання для верифікації профілю було надіслано Вам на e-mail',

];
