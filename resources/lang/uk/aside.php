<?php
return [
    'documents' => 'Документи',
    'groups' => 'Групи',
    'users' => 'Користувачі',
    'templates' => 'Шаблони',
    'doc_types' => 'Типи документів',
    'upload' => 'Завантажити',
    'create' => 'Створити з шаблону',
    'income' => 'Вхідні',
    'published' => 'Опубліковані',
    'vision' => 'Візування',
];