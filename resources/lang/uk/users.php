<?php
return [
    'name' => 'Ім\'я',
    'group' => 'Група',
    'email' => 'E-mail',
    'status' => 'Статус',
    'appoint' => 'Призначити',
    'block'=>'Заблокувати',
    'unblock'=>'Розблокувати',
    'admin'=>'Адміністратор',
    'head_dep'=>'Завідувач',
    'head_group'=>'Староста',
    'appoint_head_dep'=>'Завідувачем',
    'appoint_head_group'=>'Старостою',
    'fire'=>'Зняти з посади',
    'tutor'=>'Викладач',
    'student'=>'Студент',
    'choose'=>'Обрати користувача',
];