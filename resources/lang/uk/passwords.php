<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Паролі повинні містити принаймні вісім символів та відповідати підтвердженню.',
    'reset' => 'Ваш пароль було скинуто!',
    'sent' => 'Посилання для скидання паролю було надіслано електронною поштою.',
    'token' => 'Посилання для скидання паролю не дійсне.',
    'user' => "Користувача з вказаною електроною поштою не знайдено",

];
