<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App;
use DB;

class AdditionalFileAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()) {

            $add_doc = App\AdditionalFilesDocuments::find($request->route('id'));

            if(count(App\Documents::UserDocumentPermission(Auth::user()->id, $add_doc->document_id)) or count(App\DocumentSends::UserDocumentPermission(Auth::user()->id, $add_doc->document_id)) or count(App\DocumentPublishes::UserDocumentPermission(Auth::user()->group, $add_doc->document_id)) or count(App\DocumentVisions::UserDocumentPermission(Auth::user()->id, $add_doc->document_id))){
                return $next($request);
            }else{
                return redirect('/');
            }
        }

        return redirect('/');
    }
}
