<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;
use App;

class FileAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()) {

            $doc_id = $request->route('id');

            if(count(App\Documents::UserDocumentPermission(Auth::user()->id, $doc_id)) or count(App\DocumentSends::UserDocumentPermission(Auth::user()->id, $doc_id)) or count(App\DocumentPublishes::UserDocumentPermission(Auth::user()->group, $doc_id)) or count(App\DocumentVisions::UserDocumentPermission(Auth::user()->id, $doc_id))){
                return $next($request);
            }else{
                return redirect('/');
            }
        }

        return redirect('/');
    }
}
