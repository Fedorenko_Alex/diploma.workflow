<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App;
use ZipArchive;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;

class DocumentsController extends Controller
{
    public function document_additional(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'template_id' => 'required|integer',
        ]);

        if (!$validator->fails()) {

            $fields = App\DocumentTemplates::GetTemplatesAttributes($request->input('template_id'));
            $files = App\AdditionalFilesTemplates::GetTemplatesAdditionalFiles($request->input('template_id'));
            return response()->json([
                'fields' => $fields['attributes'],
                'files' => json_encode($files),
                'load_lang' => __('aside.upload')
            ]);
        }
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type_id' => 'required:integer'
        ]);
        if ($validator->fails()) {
            session(['upload' => 1]);
            return redirect('/home')
                ->withErrors($validator)
                ->withInput();
        } else {
            $tamplate = App\DocumentTemplates::GetTemplateById($request->input('type_id'));

            $path_to_template = storage_path('app/uploads/templates/') . $tamplate->template_file_path;

            $document_name = $tamplate->name . '_' . date('d.m.Y.H.i.s');
            $file_name = 'aknt_' . $document_name . '.docx';

            $path_to_document = storage_path('app/uploads/documents/') . $file_name;

            if (file_exists($path_to_template)) {
                if (copy($path_to_template, $path_to_document)) {
                    if (chmod($path_to_document, fileperms($path_to_template))) {
                        if (extension_loaded('zip')) {

                            $arr = [];

                            foreach (json_decode($tamplate->attributes) as $attribute) {
                                $arr[$attribute->tag] = $request->input($attribute->tag);
                            }

                            $mask = array_keys($arr);
                            $replace = array_values($arr);

                            $zip_val = new ZipArchive;

                            if ($zip_val->open($path_to_document) == true) {
                                $this->replaceTags($zip_val, $mask, $replace);
                                $this->replaceTags($zip_val, $mask, $replace, 'word/footer1.xml');
                                $this->replaceTags($zip_val, $mask, $replace, 'word/footer2.xml');
                                $this->replaceTags($zip_val, $mask, $replace, 'word/footer3.xml');
                                $this->replaceTags($zip_val, $mask, $replace, 'word/footer4.xml');
                                $zip_val->close();

                                $doc_id = App\Documents::CreateDocument(
                                    [
                                        'type_id' => $request->input('type_id'),
                                        'name' => $document_name,
                                        'comment' => $request->input('comment'),
                                        'owner_id' => Auth::user()->id,
                                        'file_name' => $file_name,
                                        'created_at' => date("Y-m-d H:i:s"),
                                        'updated_at' => date("Y-m-d H:i:s"),
                                    ]
                                );

                                $files = App\AdditionalFilesTemplates::GetTemplatesAdditionalFiles($tamplate->id);

                                foreach ($files as $file) {

                                    $template_file = $request->file('template_file_' . $file['id']);
                                    $file_original_name = time() . '_' . $template_file->getClientOriginalName();
                                    $template_file->storeAs('uploads/documents', $file_original_name);

                                    App\AdditionalFilesDocuments::CreateDocumentsAdditionalFile(
                                        [
                                            'template_file_id' => $file['id'],
                                            'document_id' => $doc_id,
                                            'file_name' => $file_original_name,
                                            'created_at' => date("Y-m-d H:i:s"),
                                            'updated_at' => date("Y-m-d H:i:s"),
                                        ]
                                    );
                                }

                                return redirect('/home');

                            }
                        }
                    }
                }
            }
        }
    }

    private function replaceTags($zip_val, $mask, $replace, $key_file_name = 'word/document.xml')
    {

        $message = $zip_val->getFromName($key_file_name, 0);

        if ($message) {

            $message = str_replace($mask, $replace, $message);
            $zip_val->addFromString($key_file_name, $message);

        }


    }

    public function upload(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'system_name' => ['required', 'string', 'max:255'],
            'type_id' => ['required', 'integer'],
            'users_file' => ['required'],
        ]);

        if ($validator->fails()) {
            session(['upload' => 2]);
            return redirect('/templates')
                ->withErrors($validator)
                ->withInput();
        } else {

            $template_file = $request->file('users_file');
            $file_original_name = 'aknt_' . time() .'_'. $template_file->getClientOriginalName();
            $template_file->storeAs('uploads/documents', $file_original_name);


            App\Documents::CreateDocument(
                [
                    'type_id' => $request->input('type_id'),
                    'name' => $request->input('system_name'),
                    'comment' => $request->input('comment'),
                    'owner_id' => Auth::user()->id,
                    'file_name' => $file_original_name,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

            return redirect('/home');
        }
    }

    public function download($id)
    {
        $document = App\Documents::find($id);
        return Storage::download('uploads/documents/' . $document->file_name);

    }

    public function download_additional($id)
    {
        $document = App\AdditionalFilesDocuments::find($id);
        return Storage::download('uploads/documents/' . $document->file_name);

    }

    public function send(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer|exists:users,id',
        ]);

        if ($validator->fails()) {
            session(['modal' => 'send_' . $id]);
            return redirect('/home')
                ->withErrors($validator)
                ->withInput();
        } else {

            $user = App\User::findOrFail($request->input('user_id'));
            $document = App\Documents::findOrFail($id);

            App\DocumentSends::CreateSend(
                [
                    'document_id' => $id,
                    'receiver_id' => $request->input('user_id'),
                    'comment' => $request->input('comment'),
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                ]
            );

            Mail::send('vendor.notifications.income', ['name'=>$user->name,'doc_name'=>$document->name,'sender_name'=>Auth::user()->name], function ($message) use ($user) {

                $message->from('aknt.knu@gmail.com', 'Кафедра АКНТ');

                $message->to($user->email);
            });

            return redirect('/home');
        }
    }

    public function income()
    {
        $income = App\DocumentSends::GetUserSendsFullInfo(Auth::user()->id);

        foreach($income as $document){
            $document->additional = App\AdditionalFilesDocuments::GetDocumentsAdditionalFiles($document->id);
        }

        return view('pages.income', compact('income'));
    }

    public function publish(Request $request, $id)
    {
        $groups = [];

        for ($i = 0; $i < count($_POST['groups']); $i++) {
            $groups[$_POST['groups'][$i]] = 1;
        }

        App\DocumentPublishes::CreatePublish(
            [
                'document_id' => $id,
                'viewers' => json_encode($groups),
                'comment' => $request->input('comment'),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        );
        return redirect('/home');

    }

    public function published()
    {
        $published = App\DocumentPublishes::GetGroupsPublish(Auth::user()->group);

        foreach($published as $document){
            $document->additional = App\AdditionalFilesDocuments::GetDocumentsAdditionalFiles($document->id);
        }

        return view('pages.published', compact('published'));
    }

    public function vision(Request $request, $id)
    {
        $queue = [];

        for ($i = 0; $i <=$request->input('queue_length'); $i++) {
            $queue[$request->input('user_queue_' . $i)] = 0;
        }

        App\DocumentVisions::CreateVision(
            [
                'document_id' => $id,
                'route' => json_encode($queue),
                'comment' => $request->input('comment'),
                'next_viewer' => $request->input('user_queue_0'),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        );

        $user = App\User::findOrFail($request->input('user_queue_0'));
        $document = App\Documents::findOrFail($id);

        Mail::send('vendor.notifications.vision', ['name'=>$user->name,'doc_name'=>$document->name,'sender_name'=>Auth::user()->name], function ($message) use ($user) {

            $message->from('aknt.knu@gmail.com', 'Кафедра АКНТ');

            $message->to($user->email);
        });

        return redirect('/home');

    }

    public function visions()
    {
        $pending = App\DocumentVisions::GetUsersPendingVisions(Auth::user()->id);

        foreach($pending as $document){
            $document->additional = App\AdditionalFilesDocuments::GetDocumentsAdditionalFiles($document->id);

            $document->route = json_decode($document->route);

            foreach($document->route as $user=>$date){
                $document->route->$user = [
                    'name'=>App\User::GetUsersName($user),
                    'date'=>$date
                ];
            }
        }

        $income = App\DocumentVisions::GetUsersIncomeVisions(Auth::user()->id);

        foreach($income as $document){
            $document->additional = App\AdditionalFilesDocuments::GetDocumentsAdditionalFiles($document->id);

            $document->route = json_decode($document->route);

            foreach($document->route as $user=>$date){
                $document->route->$user = [
                    'name'=>App\User::GetUsersName($user),
                    'date'=>$date
                ];
            }
        }

        $complete = App\DocumentVisions::GetUsersCompleteVisions(Auth::user()->id);

        foreach($complete as $document){
            $document->additional = App\AdditionalFilesDocuments::GetDocumentsAdditionalFiles($document->id);

            $document->route = json_decode($document->route);

            foreach($document->route as $user=>$date){
                $document->route->$user = [
                    'name'=>App\User::GetUsersName($user),
                    'date'=>$date
                    ];
            }
        }

        return view('pages.visions', compact('pending', 'income', 'complete'));
    }

    public function vision_approve($id)
    {
        $vision = App\DocumentVisions::find($id);

        $route = json_decode($vision->route, true);

        $users = array_keys($route);
        $i = 0;

        foreach ($route as $user => $val) {
            if ($user == Auth::user()->id) {
                $route[$user] = date("Y-m-d H:i:s");
                $vision->route = json_encode($route);
                if (($i+1) !== count($route)) {
                    $next_user = $users[$i + 1];
                    $vision->next_viewer = $next_user;

                    $user = App\User::findOrFail($next_user);
                    $document = App\Documents::findOrFail($vision->document_id);

                    Mail::send('vendor.notifications.vision', ['name'=>$user->name,'doc_name'=>$document->name,'sender_name'=>Auth::user()->name], function ($message) use ($user) {

                        $message->from('aknt.knu@gmail.com', 'Кафедра АКНТ');

                        $message->to($user->email);
                    });

                } else {
                    $vision->status = 10;
                }
                $vision->save();
                return response()->json([
                    'result' => 'success'
                ]);
            }
            $i++;
        }
    }

    public function vision_reject($id)
    {
        $vision = App\DocumentVisions::find($id);
        $vision->status = 0;
        $vision->save();
        return response()->json([
            'result' => 'success'
        ]);
    }
}
