<?php

namespace App\Http\Controllers;

use App\AdditionalFilesTemplates;
use App\Documents;
use App\DocumentTemplates;
use App\DocumentTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Support\Facades\Auth;

class TemplatesController extends Controller
{
    public function index(){
        $templates = DocumentTemplates::GetTemplatesWithRelatedTypes();


        foreach($templates as $key=>$template){
            $templates[$key]->attributes = json_decode($template->attributes,true);
            $templates[$key]->attributes_count = count((array)$templates[$key]->attributes);
            $templates[$key]->visible_for = json_decode($template->visible_for, true);

        }

        $types = DocumentTypes::all();

        $files_all = AdditionalFilesTemplates::all();

        $files_sorted = [];

        foreach($files_all as $file){
            if(isset($files_sorted[$file->template_id])){
                array_push($files_sorted[$file->template_id], $file);
            }else{
                $files_sorted[$file->template_id] = [$file];
            }
        }


        return view('pages.templates', compact('templates', 'types', 'files_sorted'));
    }

    public function edit(Request $request, $id = 0)
    {

        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'type_id' => ['required', 'integer'],
        ];

        if(!$id){
            $rules['template_file'] = ['required', 'max:10000'];
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            session(['template' => $id]);
            return redirect('/templates')
                ->withErrors($validator)
                ->withInput();
        } else {
            if ($id) {
                DocumentTemplates::UpdateTemplate([
                    'id'=> $id,
                    'name' => $request->input('name'),
                    'type_id' => $request->input('type_id'),
                    'comment' => $request->input('comment'),
                    'visibility' => isset($_POST['visibility'])?1:0,
                ]);
            } else {

                $template_file = $request->file('template_file');
                $file_original_name = time().'_'.$template_file->getClientOriginalName();
                $template_file->storeAs('uploads/templates',$file_original_name);

                $fields = [];

                for($i=1; $i<=$request->input('fields_num'); $i++){
                    $fields[] = ['tag'=>$request->input('field_tag_'.$i),'description'=>$request->input('field_description_'.$i)];
                }

                $fileds = json_encode($fields);

                $available = [];


                if (isset($_POST['available']) && count($_POST['available'])) {
                    for ($i = 0; $i < count($_POST['available']); $i++) {
                        $available[$_POST['available'][$i]] = 1;
                    }
                }

                $available = json_encode($available);


                $template_id = DocumentTemplates::CreateTemplate([
                    'name' => $request->input('name'),
                    'type_id' => $request->input('type_id'),
                    'comment' => $request->input('comment'),
                    'visibility' => isset($_POST['visibility'])?1:0,
                    'visible_for' => $available,
                    'template_file_path' => $file_original_name,
                    'attributes' => $fileds,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                ]);

                for($i=1; $i<=$request->input('files_num'); $i++){

                    AdditionalFilesTemplates::CreateAdditionalFileTemplate(
                        [
                            'template_id' => $template_id,
                            'name' => $request->input('file_description_'.$i),
                            'required' => (isset($_POST['file_required_'.$i])?1:0),
                            'created_at' => date("Y-m-d H:i:s"),
                            'updated_at' => date("Y-m-d H:i:s"),
                        ]
                    );
                }
            }
            Session::forget('file_types');
            Session::push('file_types', Documents::user_available_types(Auth::user()));

            return redirect('/templates');
        }
    }
}
