<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $documents = App\Documents::GetDocumentsFullInfo(Auth::user()->id);

        foreach($documents as $document){
            $document->additional = App\AdditionalFilesDocuments::GetDocumentsAdditionalFiles($document->id);
        }

        $users = App\User::all()->except(Auth::id());

        $groups = App\Groups::all();

        return view('pages.documents', compact('documents', 'users', 'groups'));
    }
}
