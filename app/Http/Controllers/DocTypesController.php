<?php

namespace App\Http\Controllers;

use App\Documents;
use App\DocumentTemplates;
use App\DocumentTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DocTypesController extends Controller
{
    public function index()
    {
        $doc_types = DocumentTypes::all();

        $docs = DocumentTemplates::GetTemplatesShortInfo();

        $docs_sorted = [];

        foreach($docs as $doc){
            if(isset($docs_sorted[$doc->type_id])){
                array_push($docs_sorted[$doc->type_id], $doc);
            }else{
                $docs_sorted[$doc->type_id] = [$doc];
            }
        }

        return view('pages.docTypes', compact('doc_types', 'docs_sorted'));
    }

    public function edit(Request $request, $id = 0)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            session(['doc_type' => $id]);
            return redirect('/docTypes')
                ->withErrors($validator)
                ->withInput();
        } else {
            if ($id) {

                DocumentTypes::UpdateDocType([
                    'id'=>$id,
                    'name' => $request->input('name'),
                    'updated_at' => date("Y-m-d H:i:s"),
                ]);

            } else {
                DocumentTypes::CreateDocType([
                    'name' => $request->input('name'),
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                ]);
            }
            return redirect('/docTypes');
        }
    }

    public function delete($id)
    {
        if(!count(Documents::GetDocumentsByTypeId($id))){
            DocumentTypes::DeleteDocType($id);
        }
        return redirect('/docTypes');
    }
}
