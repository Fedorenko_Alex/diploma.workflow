<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App;

class WelcomeController extends Controller
{
    public function index(){
        if(Auth::user()){
            return redirect('home/');
        }
        $groups = App\Groups::student_groups();
        return view('welcome',compact('groups'));
    }
}
