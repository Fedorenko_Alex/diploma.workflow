<?php

namespace App\Http\Controllers;

use App\Groups;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GroupsController extends Controller
{
    public function index()
    {
        $groups = Groups::all();

        $users_all = User::GetContactData();

        $users_sorted = [];

        foreach ($users_all as $user) {
            if (isset($users_sorted[$user->group])) {
                array_push($users_sorted[$user->group], $user);
            } else {
                $users_sorted[$user->group] = [$user];
            }
        }

        return view('pages.groups', compact('groups', 'users_sorted'));
    }

    public function edit(Request $request, $id = 0)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            session(['group' => $id]);
            return redirect('/groups')
                ->withErrors($validator)
                ->withInput();
        } else {
            if ($id) {
                Groups::UpdateGroup([
                    'id' => $id,
                    'name' => $request->input('name'),
                    'updated_at' => date("Y-m-d H:i:s"),
                ]);
            } else {
                Groups::CreatGroup([
                    'name' => $request->input('name'),
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                ]);
            }
            return redirect('/groups');
        }
    }

    public function delete($id)
    {
        if (!count(User::GetGroupsUsres($id))) {
            Groups::DeleteGroup($id);
        }
        return redirect('/groups');
    }
}
