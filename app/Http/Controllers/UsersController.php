<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::GetUsersWithGroups();
        foreach ($users as $user) {
            if ($user->role != 'admin') {
                $app = $users->where('group', '=', $user->group)->whereInStrict('role', ['dep_head', 'group_head'])->first();
                $user->appoint = is_null($app) ? $app : $app->id;
            } else
                $user->appoint = 0;
        }

        return view('pages.users', compact('users'));
    }

    public function changeStatus($id)
    {
        $user = User::GetUserById($id);
        $user->status = !$user->status;
        $user->save();
        $val = ($user->status)?trans('users.block'):trans('users.unblock');
        return response()->json([
            'status' => $user->status,
            'value' => $val,
        ]);
    }

    public function appoint($id)
    {
        $user = User::GetUserById($id);
        if ($user->role != 'admin') {
            switch ($user->role) {
                case 'dep_head':
                    $user->role = 'tutor';
                    break;
                case 'tutor':
                    $user->role = 'dep_head';
                    break;
                case 'group_head':
                    $user->role = 'student';
                    break;
                case 'student':
                    $user->role = 'group_head';
                    break;
            }
            $user->save();
        }

        return redirect('/users');
    }

}
