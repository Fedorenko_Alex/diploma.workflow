<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;

class LocaleController extends Controller
{
    public function index($locale){
        if (in_array($locale, Config::get('app.locales'))) {
            session(['locale' => $locale]);
        }
        return redirect()->back();
    }
}
