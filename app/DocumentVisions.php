<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class DocumentVisions extends Model
{
    public static function CreateVision($vision_data){
        DB::table('document_visions')->insert(
            [
                'document_id' => $vision_data['document_id'],
                'route' => $vision_data['route'],
                'comment' => $vision_data['comment'],
                'next_viewer' => $vision_data['next_viewer'],
                'created_at' => $vision_data['created_at'],
                'updated_at' => $vision_data['updated_at'],
            ]
        );
    }

    public static function GetUsersPendingVisions($user_id, $doc = null)
    {
        $where = [
            ['document_visions.status', '2'],
            ['documents.owner_id', $user_id],
        ];

        return DB::table('document_visions')
            ->join('documents', 'documents.id', '=', 'document_visions.document_id')
            ->join('document_types', 'document_types.id', '=', 'documents.type_id')
            ->where($where)
            ->select('document_visions.id as vision_id', 'documents.id', 'documents.name as doc_name', 'document_visions.comment', 'document_visions.created_at', 'document_types.name as type_name', 'document_visions.status', 'document_visions.route')
            ->orderBy('document_visions.id', 'desc')
            ->get();
    }

    public static function GetUsersIncomeVisions($user_id)
    {
        return DB::table('document_visions')
            ->where([
                ['document_visions.status', '2'],
                ['document_visions.next_viewer', $user_id],
            ])
            ->join('documents', 'documents.id', '=', 'document_visions.document_id')
            ->join('users', 'users.id', '=', 'documents.owner_id')
            ->join('document_types', 'document_types.id', '=', 'documents.type_id')
            ->select('document_visions.id as vision_id', 'documents.id', 'document_visions.id as vision_id', 'documents.name as doc_name', 'document_visions.comment', 'document_visions.created_at', 'document_types.name as type_name', 'document_visions.status', 'users.name as sender', 'document_visions.route')
            ->orderBy('document_visions.id', 'desc')
            ->get();
    }

    public static function GetUsersCompleteVisions($user_id)
    {
        return DB::table('document_visions')
            ->join('documents', 'documents.id', '=', 'document_visions.document_id')
            ->join('document_types', 'document_types.id', '=', 'documents.type_id')
            ->where([
                ['document_visions.status', '<>', '2'],
                ['documents.owner_id', $user_id],
            ])
            ->select('document_visions.id as vision_id', 'documents.id', 'documents.name as doc_name', 'document_visions.comment', 'document_visions.created_at', 'document_types.name as type_name', 'document_visions.status', 'document_visions.route', 'document_visions.next_viewer')
            ->orderBy('document_visions.id', 'desc')
            ->get();
    }

    public static function UserDocumentPermission($user_id, $document_id){
       return DB::table('document_visions')
            ->where([
                ['document_visions.status', '2'],
                ['document_visions.document_id', $document_id],
                ['document_visions.next_viewer', $user_id],
            ])
            ->join('documents', 'documents.id', '=', 'document_visions.document_id')
            ->join('users', 'users.id', '=', 'documents.owner_id')
            ->join('document_types', 'document_types.id', '=', 'documents.type_id')
            ->select('document_visions.id as vision_id','documents.id','document_visions.id as vision_id', 'documents.name as doc_name', 'document_visions.comment', 'document_visions.created_at', 'document_types.name as type_name', 'users.name as sender','document_visions.route')
            ->get();
    }

}
