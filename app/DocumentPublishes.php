<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class DocumentPublishes extends Model
{
    public static function CreatePublish($publish_data){
        DB::table('document_publishes')->insert(
            [
                'document_id' => $publish_data['document_id'],
                'viewers' => $publish_data['viewers'],
                'comment' => $publish_data['comment'],
                'created_at' => $publish_data['created_at'],
                'updated_at' => $publish_data['updated_at'],
            ]
        );
    }

    public static function GetGroupsPublish($group_id){
        return DB::table('document_publishes')
            ->whereRaw('JSON_CONTAINS(document_publishes.viewers, \'{"' . $group_id . '":1}\')')
            ->join('documents', 'documents.id', '=', 'document_publishes.document_id')
            ->join('users', 'users.id', '=', 'documents.owner_id')
            ->join('document_types', 'document_types.id', '=', 'documents.type_id')
            ->select('documents.id','documents.name as doc_name', 'document_publishes.comment', 'document_publishes.created_at', 'document_types.name as type_name', 'users.name as sender')
            ->orderBy('document_publishes.id', 'desc')
            ->get();

    }

    public static function UserDocumentPermission($group_id, $document_id){
        return DB::table('document_publishes')
            ->whereRaw('JSON_CONTAINS(document_publishes.viewers, \'{"' .$group_id . '":1}\') and document_id='.$document_id)
            ->join('documents', 'documents.id', '=', 'document_publishes.document_id')
            ->join('users', 'users.id', '=', 'documents.owner_id')
            ->join('document_types', 'document_types.id', '=', 'documents.type_id')
            ->select('documents.id','documents.name as doc_name', 'document_publishes.comment', 'document_publishes.created_at', 'document_types.name as type_name', 'users.name as sender')
            ->get();
    }
}
