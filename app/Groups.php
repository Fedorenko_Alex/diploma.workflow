<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Groups extends Model
{
    public static function student_groups()
    {
        return static::where('id', '>', 1)->get();
    }

    public static function CreatGroup($group_data)
    {
        DB::table('groups')->insert([
                'name' => $group_data['name'],
                'created_at' => $group_data['created_at'],
                'updated_at' => $group_data['updated_at'],
            ]
        );
    }

    public static function UpdateGroup($group_data)
    {
        DB::table('groups')
            ->where('id', $group_data['id'])
            ->update([
                    'name' => $group_data['name'],
                    'updated_at' => $group_data['updated_at']
                ]
            );
    }

    public static function DeleteGroup($id){
        return static::where('id', '=', $id)->delete();
    }
}
