<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class AdditionalFilesDocuments extends Model
{
    public static function GetDocumentsAdditionalFiles($document_id){
        return DB::table('additional_files_documents')
            ->where('document_id', $document_id)
            ->join('additional_files_templates', 'additional_files_documents.template_file_id', '=', 'additional_files_templates.id')
            ->select('additional_files_documents.id', 'additional_files_templates.name')
            ->get();
    }

    public static function CreateDocumentsAdditionalFile($file_data){
        DB::table('additional_files_documents')->insert(
            [
                'template_file_id' => $file_data['template_file_id'],
                'document_id' => $file_data['document_id'],
                'file_name' => $file_data['file_name'],
                'created_at' => $file_data['created_at'],
                'updated_at' => $file_data['updated_at'],
            ]
        );
    }
}
