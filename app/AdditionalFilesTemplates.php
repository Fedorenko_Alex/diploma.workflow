<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class AdditionalFilesTemplates extends Model
{
    public static function GetTemplatesAdditionalFiles($template_id){
        return static::where('template_id', $template_id)->select('id', 'name')->get()->toArray();
    }

    public static function CreateAdditionalFileTemplate($file_data){
        DB::table('additional_files_templates')->insert(
            [
                'template_id' => $file_data['template_id'],
                'name' => $file_data['name'],
                'required' => $file_data['required'],
                'created_at' => $file_data['created_at'],
                'updated_at' => $file_data['updated_at'],
            ]
        );
    }
}
