<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class DocumentTypes extends Model
{
    public static function CreateDocType($type_data){
        DB::table('document_types')
            ->insert([
                    'name' => $type_data['name'],
                    'created_at' => $type_data['created_at'],
                    'updated_at' => $type_data['updated_at']
                ]
            );
    }

    public static function UpdateDocType($type_data){
        DB::table('document_types')
            ->where('id', $type_data['id'])
            ->update([
                'name' => $type_data['name'],
                'updated_at' => $type_data['updated_at']
            ]);
    }

    public static function DeleteDocType($id){
        return static::where('id', '=', $id)->delete();
    }
}
