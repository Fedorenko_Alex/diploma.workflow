<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Documents extends Model
{
    public static function user_available_types($user){

        if($user->role === 'admin'){
            return DB::table('document_templates')
                ->select('id','name')
                ->get();
        }else{
            return DB::table('document_templates')
                ->whereRaw('JSON_CONTAINS(document_templates.visible_for, \'{"'.$user->number_role().'":1}\') and visibility=1')
                ->select('id','name')
                ->get();
        }
    }

    public static function GetDocumentsFullInfo($user){
        return DB::table('documents')
            ->where('owner_id', $user)
            ->join('document_types', 'document_types.id', '=', 'documents.type_id')
            ->join('users', 'users.id', '=', 'documents.owner_id')
            ->select('documents.*', 'document_types.name as type_name')
            ->orderBy('id', 'desc')
            ->get();
    }

    public static function GetDocumentsByTypeId($id)
    {
        return static::where('type_id', $id)->get();
    }

    public static function CreateDocument($doc_data){
        return DB::table('documents')->insertGetId(
            [
                'type_id' => $doc_data['type_id'],
                'name' => $doc_data['name'],
                'comment' => $doc_data['comment'],
                'owner_id' => $doc_data['owner_id'],
                'file_name' => $doc_data['file_name'],
                'created_at' => $doc_data['created_at'],
                'updated_at' => $doc_data['updated_at'],
            ]
        );
    }

    public static function UserDocumentPermission($user_id, $document_id){
       return DB::table('documents')
            ->where([
                ['documents.owner_id', $user_id],
                ['documents.id',$document_id]
            ])
            ->join('document_types', 'document_types.id', '=', 'documents.type_id')
            ->join('users', 'users.id', '=', 'documents.owner_id')
            ->select('documents.*', 'document_types.name as type_name')
            ->get();
    }

}
