<?php

namespace App;

use App\Notifications\ResetPass;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'group', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * Send the password reset notification.
     *
     * @param string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPass($token));
    }

    public function isAdmin()
    {

        return $this->attributes['role'] == 'admin';
    }

    public function number_role()
    {
        switch ($this->attributes['role']) {
            case 'admin':
                return 1;
            case 'dep_head':
                return 2;
            case 'tutor':
                return 3;
            case 'group_head':
                return 4;
            case 'student':
                return 5;
            default:
                return 0;
        }
    }

    public static function GetUsersWithGroups()
    {
        return DB::table('users')
            ->join('groups', 'users.group', '=', 'groups.id')
            ->select('users.*', 'groups.name as group_name')->get();
    }

    public static function GetUserById($id)
    {
        return static::where('id', $id)->first();
    }

    public static function GetContactData(){
        return static::select('name','group','email')->get();
    }

    public static function GetGroupsUsres($group_id){
        return static::where('group', $group_id)->get();
    }

    public static function GetUsersName($id){
        return static::where('id', $id)->value('name');
    }
}
