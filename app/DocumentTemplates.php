<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class DocumentTemplates extends Model
{
    public static function GetTemplateById($id){
        return static::where('id', $id)->first();
    }

    public static function GetTemplatesWithRelatedTypes(){
        return DB::table('document_templates')
            ->join('document_types', 'document_types.id', '=', 'document_templates.type_id')
            ->select('document_templates.*','document_types.name as type_name')
            ->get();
    }

    public static function GetTemplatesShortInfo(){
        return static::select('name','type_id','comment')->get();
    }

    public static function CreateTemplate($tpl_data){
        return DB::table('document_templates')->insertGetId(
            [
                'name' => $tpl_data['name'],
                'type_id' => $tpl_data['type_id'],
                'comment' => $tpl_data['comment'],
                'visibility' => $tpl_data['visibility'],
                'visible_for' => $tpl_data['visible_for'],
                'template_file_path' => $tpl_data['template_file_path'],
                'attributes' => $tpl_data['attributes'],
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        );
    }

    public static function UpdateTemplate($tpl_data){
        DB::table('document_templates')
            ->where('id', $tpl_data['id'])
            ->update([
                'name' => $tpl_data['name'],
                'type_id' => $tpl_data['type_id'],
                'comment' => $tpl_data['comment'],
                'visibility' => $tpl_data['visibility'],
            ]);
    }

    public static function GetTemplatesAttributes($template_id){
        return static::where('id', $template_id)->select('attributes')->first()->toArray();
    }
}
