<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class DocumentSends extends Model
{
    public static function CreateSend($send_data){
        DB::table('document_sends')->insert(
            [
                'document_id' => $send_data['document_id'],
                'receiver_id' => $send_data['receiver_id'],
                'comment' => $send_data['comment'],
                'created_at' => $send_data['created_at'],
                'updated_at' => $send_data['updated_at'],
            ]
        );
    }

    public static function GetUserSendsFullInfo($user_id){
        return DB::table('document_sends')
            ->where('receiver_id', $user_id)
            ->join('documents', 'documents.id', '=', 'document_sends.document_id')
            ->join('users', 'users.id', '=', 'documents.owner_id')
            ->join('document_types', 'document_types.id', '=', 'documents.type_id')
            ->select('documents.id','documents.name as doc_name', 'document_sends.comment', 'document_sends.created_at', 'document_types.name as type_name', 'users.name as sender')
            ->orderBy('document_sends.id', 'desc')
            ->get();
    }

    public static function UserDocumentPermission($user_id, $document_id){
        return DB::table('document_sends')
            ->where([
                ['document_sends.receiver_id', $user_id],
                ['document_sends.document_id',$document_id]
            ])
            ->join('documents', 'documents.id', '=', 'document_sends.document_id')
            ->join('users', 'users.id', '=', 'documents.owner_id')
            ->join('document_types', 'document_types.id', '=', 'documents.type_id')
            ->select('documents.id','documents.name as doc_name', 'document_sends.comment', 'document_sends.created_at', 'document_types.name as type_name', 'users.name as sender')
            ->get();
    }
}
