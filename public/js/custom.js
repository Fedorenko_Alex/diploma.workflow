let fields = 1;
let files = 1;

$('.switch_welcome_form').click(function () {
    $('.log').toggleClass('d-none');
    $('.reg').toggleClass('d-none');
});

$(document).ready(function () {

    var docHeight = $(window).height();
    var footer = $('footer');
    var footerHeight = footer.height();
    var footerTop = footer.position().top + footerHeight;

    if (footerTop < docHeight)
        footer.css('margin-top', (docHeight - footerTop) + 'px');
});

function switchAside(i) {
    $('.aknt_aside li a').removeClass('active');
    $('.aknt_aside li:nth-child(' + i + ') a').addClass('active');
}

$('button[data-reload]').click(function () {
    window.location.href = $(this).attr('data-reload');
});

$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

});

$(".users-status").click(function (e) {

    e.preventDefault();

    var obj = $(this);

    $.ajax({

        type: 'GET',

        url: $(this).attr('data-url'),

        success: function (data) {
            console.log(data);
            obj.html(data.value);
            if (data.status === true) {
                obj.removeClass('text-warning').addClass('text-success');
            } else if (data.status === false) {
                obj.removeClass('text-success').addClass('text-warning');
            }

        }

    });


});

$('.add_field').click(function (e) {
    e.preventDefault();
    let markup = '<div class="row mb-3">' +
        '    <div class="col">' +
        '      <input type="text" class="form-control" name="field_description_' + fields + '" placeholder="' + $(this).data('description') + '">' +
        '    </div>' +
        '    <div class="col">' +
        '      <input type="text" class="form-control" name="field_tag_' + fields + '" placeholder="' + $(this).data('tag') + '">' +
        '    </div>' +
        '  </div>';
    $(this).parent().before(markup);
    $('input[name="fields_num"]').val(fields);
    fields++;
});

$('.add_file').click(function (e) {
    e.preventDefault();
    let markup = '<div class="row mb-3">' +
        '    <div class="col">' +
        '      <input type="text" class="form-control" name="file_description_' + files + '" placeholder="' + $(this).data('description') + '">' +
        '    </div>' +
        '    <div class="col d-flex align-items-center justify-content-center custom-control custom-checkbox">' +
        '      <input type="checkbox" class="custom-control-input" id="file_required_' + files + '" name="file_required_' + files + '">' +
        '      <label class="custom-control-label" for="file_required_' + files + '">' + $(this).data('required') + '</label> ' +
        '    </div>' +
        '  </div>';
    $(this).parent().before(markup);
    $('input[name="files_num"]').val(files);
    files++;
});

$(document).delegate('input[type="file"]', "change", function () {
    let fileName = $(this).val();
    $(this).next('.custom-file-label').html(fileName);
});

$('#from_template select[name="type_id"]').on('change', function () {
    var obj = $(this);

    $.ajax({
        type: 'POST',
        url: $(this).attr('data-url'),
        data: {"template_id": obj.val()},
        success: function (data) {
            let fields = JSON.parse(data.fields);
            let files = JSON.parse(data.files);
            let files_string = fields_string = '';

            $.each(fields, function (key, val) {
                fields_string += '<div class="form-group">' +
                    '<label for="' + val.tag + '">' + val.description + '</label>' +
                    '<input type="text" name="' + val.tag + '" class="form-control">' +
                    '</div>';
            });
            $('#template_fields').html(fields_string);

            $.each(files, function (key, val) {
                files_string += '<label>' + val.name + '</label>' +
                    '<div class="custom-file mb-3">' +
                    '<input type="file" class="custom-file-input" id="template_file_' + val.id + '" name="template_file_' + val.id + '">' +
                    '<label class="custom-file-label overflow-hidden " for="template_file_' + val.id + '" data-browse="' + data.load_lang + '"></label>' +
                    '</div>';
            });
            $('#template_files').html(files_string);
        }

    });
});

$('.add_queue').click(function (e) {
    e.preventDefault();

    let queue_len = $(this).parent().find('[name="queue_length"]').val();

    queue_len = parseInt(queue_len) +1;

    let form = $(this).closest('.modal').find('[name="user_queue_0"]').attr('form');

    let markup = '' +
        '<div class="form-group">' +
        '    <select name="user_queue_' + queue_len + '" class="custom-select" form="'+form+'">' +
        '    </select>' +
        '</div>';

    $(this).parent().before(markup);

    let options = $(this).closest('.modal').find('[name="user_queue_0"] option').clone();
    $(this).closest('.modal').find('[name="user_queue_' + queue_len+'"]').append(options);

    $(this).parent().find('[name="queue_length"]').val(queue_len)
});

$('.vision_action').click(function (e) {
    var obj = $(this);

    $.ajax({
        type: 'GET',
        url: $(this).attr('data-url'),
        success: function (data) {
            if(data.result === 'success'){
                obj.closest('tr').remove();
            }
        }

    });
});