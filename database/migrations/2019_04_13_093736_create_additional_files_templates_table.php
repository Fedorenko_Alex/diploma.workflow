<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdditionalFilesTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_files_templates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('template_id')->unsigned();
            $table->string('name','100');
            $table->boolean('required')->default(0);
            $table->timestamps();

            $table->index('template_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('additional_files_templates', function(Blueprint $table)
        {
            $table->dropIndex('additional_files_templates_template_id_index');

        });
        Schema::dropIfExists('additional_files_templates');
    }
}
