<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('status')->default(1);
            $table->enum('role',['admin', 'dep_head', 'tutor', 'group_head', 'student'])->comment('1-admin; 2-Head of Department; 3-Tutor; 4-Group headman; 5-student');
            $table->bigInteger('group')->unsigned();
            $table->rememberToken();
            $table->timestamps();

            $table->index('group');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $table->dropIndex('users_group_index');

        });
        Schema::dropIfExists('users');
    }
}
