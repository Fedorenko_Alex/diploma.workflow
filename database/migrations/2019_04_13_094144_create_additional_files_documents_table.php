<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdditionalFilesDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_files_documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('template_file_id')->unsigned();
            $table->bigInteger('document_id')->unsigned();
            $table->string('file_name', 150);
            $table->timestamps();

            $table->unique(['template_file_id', 'document_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('additional_files_documents', function(Blueprint $table)
        {
            $table->dropUnique('additional_files_documents_template_file_id_document_id_unique');

        });
        Schema::dropIfExists('additional_files_documents');
    }
}
