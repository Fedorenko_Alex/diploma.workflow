<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('group')->references('id')->on('groups');
        });
        Schema::table('document_templates', function (Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('document_types');
        });
        Schema::table('documents', function (Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('document_types');
            $table->foreign('owner_id')->references('id')->on('users');
        });
        Schema::table('additional_files_templates', function (Blueprint $table) {
            $table->foreign('template_id')->references('id')->on('document_templates');
        });
        Schema::table('additional_files_documents', function (Blueprint $table) {
            $table->foreign('template_file_id')->references('id')->on('additional_files_templates');
            $table->foreign('document_id')->references('id')->on('documents');
        });
        Schema::table('document_sends', function (Blueprint $table) {
            $table->foreign('receiver_id')->references('id')->on('users');
            $table->foreign('document_id')->references('id')->on('documents');
        });
        Schema::table('document_visions', function (Blueprint $table) {
            $table->foreign('next_viewer')->references('id')->on('users');
            $table->foreign('document_id')->references('id')->on('documents');
        });
        Schema::table('document_publishes', function (Blueprint $table) {
            $table->foreign('document_id')->references('id')->on('documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['group']);
        });
        Schema::table('document_templates', function (Blueprint $table) {
            $table->dropForeign(['type_id']);
        });
        Schema::table('documents', function (Blueprint $table) {
           $table->dropForeign(['type_id']);
            $table->dropForeign(['owner_id']);
        });
        Schema::table('additional_files_templates', function (Blueprint $table) {
            $table->dropForeign(['template_id']);
        });
        Schema::table('additional_files_documents', function (Blueprint $table) {
            $table->dropForeign(['template_file_id']);
            $table->dropForeign(['document_id']);
        });
        Schema::table('document_sends', function (Blueprint $table) {
            $table->dropForeign(['receiver_id']);
            $table->dropForeign(['document_id']);
        });
        Schema::table('document_visions', function (Blueprint $table) {
            $table->dropForeign(['next_viewer']);
            $table->dropForeign(['document_id']);
        });
        Schema::table('document_publishes', function (Blueprint $table) {
            $table->dropForeign(['document_id']);
        });
    }
}
