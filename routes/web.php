<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'WelcomeController@index');

Route::get('setlocale/{locale}', 'LocaleController@index');

Auth::routes(['verify' => true]);

Route::middleware('verified')->group(function () {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::post('/create','DocumentsController@create');
    Route::post('/document_additional','DocumentsController@document_additional');
    Route::post('/upload','DocumentsController@upload');
    Route::get('/download/{id}','DocumentsController@download')->middleware('FileAccess');
    Route::get('/download/additional/{id}','DocumentsController@download_additional')->middleware('AdditionalFileAccess');

    Route::get('/income', 'DocumentsController@income');
    Route::post('/send/{id}', 'DocumentsController@send');
    Route::post('/delete/{id}', 'DocumentsController@delete');

    Route::get('/published', 'DocumentsController@published');
    Route::post('/publish/{id}', 'DocumentsController@publish');

    Route::get('/visions', 'DocumentsController@visions');
    Route::post('/vision/{id}', 'DocumentsController@vision');
    Route::get('/vision/approve/{id}', 'DocumentsController@vision_approve');
    Route::get('/vision/reject/{id}', 'DocumentsController@vision_reject');
});

Route::middleware('IsAdmin')->group(function () {

    Route::get('/users', 'UsersController@index');
    Route::get('/users/block/{id}', 'UsersController@changeStatus');
    Route::get('/users/appoint/{id}', 'UsersController@appoint');

    Route::get('/groups', 'GroupsController@index');
    Route::post('/groups/edit/{id}', 'GroupsController@edit');
    Route::get('/groups/delete/{id}', 'GroupsController@delete');

    Route::get('/docTypes', 'DocTypesController@index');
    Route::post('/docTypes/edit/{id}', 'DocTypesController@edit');
    Route::get('/docTypes/delete/{id}', 'DocTypesController@delete');

    Route::get('/templates', 'TemplatesController@index');
    Route::post('/templates/edit/{id}', 'TemplatesController@edit');
    Route::get('/templates/delete/{id}', 'TemplatesController@delete');


});



